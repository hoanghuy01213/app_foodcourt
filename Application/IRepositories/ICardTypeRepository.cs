﻿using Application.Repositories;
using Domain.Entities;

namespace Application.IRepositories
{
    public interface ICardTypeRepository : IGenericRepository<CardType>
    {
    }
}
