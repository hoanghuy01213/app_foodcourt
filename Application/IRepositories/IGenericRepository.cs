﻿using Application.Commons;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Application.Repositories
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>>? expression = null, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression);
        Task<T?> FirstOrDefaultAsync(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);
        Task<T?> GetEntityByIdAsync(Guid id);
        Task<T?> GetEntityByGenericAsync(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);
        Task<bool> CheckExistGeneric(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);
        Task AddEntityAsync(T entity);
        void UpdateEntity(T entity);
        void UpdateRange(IEnumerable<T> entities);
        void SoftRemoveEntity(T entity);
        Task AddEntityRange(IEnumerable<T> entities);
        void SoftRemoveEntityRange(IEnumerable<T> entities);
        Task<Pagination<T>> ToPagination(int pageNumber = 0, int pageSize = 10);
        Task<Pagination<T>> ToPagination(Expression<Func<T, bool>> filter,
                                                        int pageIndex = 0,
                                                        int pageSize = 10);
        Task<Pagination<T>> ToPagination(
          Expression<Func<T, bool>> expression = null,
          Func<IQueryable<T>, IQueryable<T>> include = null,
          int pageIndex = 0,
          int pageSize = 10);
        Task<Pagination<T>> ToPagination(Expression<Func<T, bool>> expression = null,
        Func<IQueryable<T>, IQueryable<T>> include = null,
        int pageIndex = 0,
        int pageSize = 10,
        Expression<Func<T, object>> sortColumn = null,
        SortDirection sortDirection = SortDirection.Descending);
        Task<Pagination<T>> ToPaginationNoQueryFilter(Expression<Func<T, bool>> expression = null,
        Func<IQueryable<T>, IQueryable<T>> include = null,
        int pageIndex = 0,
        int pageSize = 10,
        Expression<Func<T, object>> sortColumn = null,
        SortDirection sortDirection = SortDirection.Descending);
    }
}