﻿using Application.Commons;
using Application.Repositories;
using Domain.Entities;

namespace Application.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<Pagination<Product>> GetProductsByName(string productName, int pageIndex = 0, int pageSize = 10);
        Task<Pagination<Product>> GetProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10);
    }
}
