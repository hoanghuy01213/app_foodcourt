﻿using Application.Commons;
using Application.Repositories;
using Domain.Entities;

namespace Application.IRepositories
{
    public interface IShopCategoryRepository : IGenericRepository<ShopCategory>
    {
    }
}
