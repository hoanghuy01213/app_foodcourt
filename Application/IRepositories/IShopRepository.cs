﻿using Application.Commons;
using Application.Repositories;
using Domain.Entities;

namespace Application.IRepositories
{
    public interface IShopRepository : IGenericRepository<Shop>
    {
    }
}
