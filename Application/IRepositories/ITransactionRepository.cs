﻿using Application.Commons;
using Application.Repositories;
using Domain.Entities;

namespace Application.IRepositories
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {
    }
}
