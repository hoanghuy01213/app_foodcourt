﻿using Application.IRepositories;
using Application.Repositories;
using System.Data;

namespace Application
{
    public interface IUnitOfWork:IDisposable
    {
        public IShopRepository ShopRepository { get; }
        public IShopCategoryRepository ShopCategoryRepository { get; }
        public ITransactionCounterRepository TransactionCounterRepository { get; }
        public ICardTypeRepository CardTypeRepository { get; }
        public IProductRepository ProductRepository { get; }
        public ICardRepository CardRepository { get; }
        public IWalletRepository WalletRepository { get; }
        public IUserRepository UserRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }
        public ITransactionRepository TransactionRepository { get; }

        public Task ExecuteTransactionAsync(Action action);
        public Task<int> SaveChangesAsync();
        void BeginTransaction();
        void Commit();
        Task CommitAsync();
        void Rollback();
    }
}
