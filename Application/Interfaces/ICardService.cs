﻿using Application.Commons;
using Application.ViewModels.CardViewModels;
using Application.ViewModels.Response;

namespace Application.Interfaces
{
    public interface ICardService
    {
        Task<ResponseView<Pagination<CardViewModel>>> GetCurrentCard(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<CardViewModel>>> GetHistoryCard(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<CardViewModel>> GetCardById(Guid cardId);
        Task<ResponseView<Pagination<CardViewModel>>> GetCardsByCardTypeId(Guid cardTypeId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<CardViewModel>> CreateCard(CreateCardViewModel model);
        Task<ResponseView<CardViewModel>> UpdateCard(Guid cardId, UpdateCardViewModel model);
        Task<ResponseView<CardViewModel>> DeleteCard(Guid cardId);
    }
}
