﻿using Application.Commons;
using Application.ViewModels.CardTypeViewModels;
using Application.ViewModels.Response;

namespace Application.Interfaces
{
    public interface ICardTypeService
    {
        Task<ResponseView<Pagination<CardTypeViewModel>>> GetCurrentCardType(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<CardTypeViewModel>>> GetHistoryCardType(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<CardTypeViewModel>> GetCardTypeById(Guid cardTypeId);
        Task<ResponseView<CardTypeViewModel>> CreateCardType(CreateCardTypeViewModel cardTypeViewModel);
        Task<ResponseView<CardTypeViewModel>> UpdateCardType(Guid cardTypeId, CreateCardTypeViewModel cardType);
        Task<ResponseView<CardTypeViewModel>> DeleteCardType(Guid cardTypeId);
    }
}
