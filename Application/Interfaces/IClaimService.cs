﻿namespace Application.Interfaces
{
    public interface IClaimService
    {
        public Guid GetCurrentUserId { get; }
    }
}
