﻿using Application.Commons;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IOrderDetailService
    {
        Task<ResponseView<Pagination<OrderResponseViewModel>>> GetHistoryOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderResponseViewModel>>> GetCurrentOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderResponseViewModel>>> GetOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<OrderResponseViewModel>> AddOrder(Guid Id, CreateOrderDetailViewModel request);
        Task<ResponseView<OrderResponseViewModel>> UpdateOrderDetail(Guid Id, UpdateOrderDetailViewModel request);
        Task<ResponseView<OrderDetails>> DeleteOrderDetail(Guid orderId, Guid productId);
    }
}
