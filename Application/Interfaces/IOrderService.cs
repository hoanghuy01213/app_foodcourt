﻿using Application.Commons;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IOrderService
    {
        Task<ResponseView<Pagination<OrderViewModel>>> GetCurrentOrder(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderViewModel>>> GetHistoryOrder(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderViewModel>>> GetSuccessOrders(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderViewModel>>> GetFailOrders(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderViewModel>>> GetPendingOrders(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopViewIncludeModel>>> GetOrdersByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<OrderViewModel>>> GetOrdersByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<UserViewIncludeModel>>> GetOrdersByUserId(Guid userId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<OrderViewModel>> GetOrderById(Guid orderId);
        Task<ResponseView<OrderResponseViewModel>> CreateOrder(CreateOrderViewModel model);
        Task<ResponseView<OrderResponseViewModel>> Update(Guid orderId, UpdateOrderViewModel request);
        Task<ResponseView<ApproveOrderViewModel>> ApproveOrder(Guid orderId);
        Task<ResponseView<ApproveOrderViewModel>> RejectOrder(Guid orderId);
        Task<ResponseView<OrderViewModel>> DeleteOrder(Guid orderId);
        Task<decimal> CalculateTotalPriceAsync(IEnumerable<OrderDetail> orderDetails);

    }
}
