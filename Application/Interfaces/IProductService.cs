﻿using Application.Commons;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;

namespace Application.Interfaces
{
    public interface IProductService
    {
        Task<ResponseView<Pagination<ProductViewModel>>> GetCurrentProduct(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ProductViewModel>>> GetHistoryProduct(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ProductViewModel>>> GetHistoryProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ProductViewModel>>> GetProductsByName(string productName, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ProductViewModel>>> GetProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopViewProductViewModel>>> GetProductByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<ProductViewModel>> GetProductById(Guid productId);
        Task<ResponseView<ProductViewModel>> CreateProduct(CreateProductViewModel model);
        Task<ResponseView<ProductViewModel>> UpdateProduct(Guid productId, CreateProductViewModel model);
        Task<ResponseView<ProductViewModel>> RemoveProduct(Guid productId);
        Task<ResponseView<Pagination<TopSellingProduct>>> GetTopSellingProducts(DateTime start, DateTime end, int pageIndex = 0, int pageSize = 10);
    }
}