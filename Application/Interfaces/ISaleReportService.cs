﻿using Application.ViewModels.Response;
using Application.ViewModels.SaleReportViewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISaleReportService
    {
        Task<ResponseView<SaleReportViewModel>> GetSalesReport(DateTime startDate, DateTime endDate);
        Task<ResponseView<SaleReportViewModel>> GetSalesReport(int year, int month);
        Task<ResponseView<List<SaleReportViewModel>>> GetSalesReportByEachMonth();
    }
}
