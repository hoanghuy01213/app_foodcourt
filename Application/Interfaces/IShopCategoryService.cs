﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.ShopCategoryViewModels;

namespace Application.Interfaces
{
    public interface IShopCategoryService
    {
        Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetCurrentShopCategory(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetHistoryShopCategory(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<ShopCategoryViewModel>> GetShopCategoryById(Guid shopCateId);
        Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetShopCategoriesByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetHistoryShopCategoriesByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetShopCategoriesByName(string name, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<ShopCategoryViewModel>> CreateShopCategory(CreateShopCategoryViewModel shopCateDTO);
        Task<ResponseView<ShopCategoryViewModel>> UpdateShopCategory(Guid shopCateId, CreateShopCategoryViewModel shopCateDTO);
        Task<ResponseView<ShopCategoryViewModel>> DeleteShopCategory(Guid shopCateId);
        Task<bool> CheckShopId(Guid shopId);
    }
}
