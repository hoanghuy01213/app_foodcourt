﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IShopService
    {
        Task<ResponseView<Pagination<ShopViewModel>>> GetCurrentShop(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopViewModel>>> GetHistoryShop(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<ShopViewModel>> GetShopById(Guid shopId);
        Task<ResponseView<Pagination<ShopViewModel>>> GetShopByName(string name, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopViewModel>>> GetShopByUserId(Guid userId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<ShopViewModel>> CreateShop(CreateShopViewModel shopDTO);
        Task<ResponseView<ShopViewModel>> UpdateShop(Guid shopId, UpdateShopViewModel shopDTO);
        Task<ResponseView<ShopViewModel>> DeleteShop(Guid shopId);
        Task<ResponseView<Pagination<ShopResponseViewModel>>> GetShopDetails(Guid shopId, int pageIndex = 0, int pageSize = 10);
    }
}
