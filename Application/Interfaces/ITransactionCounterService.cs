﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.TransactionCounterViewModels;

namespace Application.Interfaces
{
    public interface ITransactionCounterService 
    {
        Task<ResponseView<Pagination<TransactionCounterViewModel>>> GetCurrentTransactionCounters(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionCounterViewModel>>> GetHistoryTransactionCounters(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionCounterViewModel>>> GetTransactionCountersByName(string transactionCounterName, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<TransactionCounterViewModel>> GetTransactionCounterById(Guid transactionCounterId);
        Task<ResponseView<TransactionCounterViewModel>> CreateTransactionCounter(CreateTransactionCounterViewModel transactionCounter);
        Task<ResponseView<TransactionCounterViewModel>> UpdateTransactionCounter(Guid transactionCounterId,  CreateTransactionCounterViewModel transactionCounter);
        Task<ResponseView<TransactionCounterViewModel>> DeleteTransactionCounter(Guid transactionCounterId);
    }
}
