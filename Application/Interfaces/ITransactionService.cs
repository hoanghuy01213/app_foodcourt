﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Application.ViewModels.TransactionViewModels;

namespace Application.Interfaces
{
    public interface ITransactionService
    {
        Task<ResponseView<Pagination<TransactionViewModel>>> GetCurrentTransaction(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetSuccessTransactions(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetFailureTransactions(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetPendingTransactions(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetTransactionsByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetTransactionsByWalletId(Guid walletId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetHistoryTransactions(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<TransactionViewModel>>> GetTransactionsByTransactionCounterId(Guid transactionCounterId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<ShopViewTransactionViewModel>>> GetTransactionsByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<TransactionViewModel>> GetTransactionById(Guid transactionId);
        Task<ResponseView<TransactionViewModel>> CreateTransaction(CreateTransactionViewModel model);
        Task<ResponseView<TransactionViewModel>> UpdateTransaction(Guid transactionId, CreateTransactionViewModel model);
        Task<ResponseView<TransactionViewModel>> DeleteTransaction(Guid transactionId);
        Task<ResponseView<ApproveTransactionViewModel>> ApproveTransaction(Guid transactionId);
        Task<ResponseView<ApproveTransactionViewModel>> RejectTransaction(Guid transactionId);
    }
}
