﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.UserViewModels;

namespace Application.Interfaces
{
    public interface IUserService
    {
        Task<ResponseView<bool>> RegisterAsync(UserRegisterDTO request);
        Task<ResponseView<UserLoginResponse>> LoginAsync(UserLoginRequest request);
        Task<ResponseView<Pagination<UserViewModel>>> GetCurrentUserAsync(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<UserViewModel>>> GetHistoryUser(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<UserViewModel>> GetUserByIdAsync(Guid userId);
        Task<ResponseView<UserUpdateRequest>> UpdateUser(Guid userId, UserUpdateRequest request);
        Task<ResponseView<ChangePasswordViewModel>> ChangePassword(Guid id, ChangePasswordViewModel changePassword);
    }
}
