﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.WalletViewModels;

namespace Application.Interfaces
{
    public interface IWalletService
    {
        Task<ResponseView<Pagination<WalletViewModel>>> GetCurrentWallet(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<WalletViewModel>>> GetHistoryWallet(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<WalletViewModel>>> GetHistoryWalletsByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<WalletViewModel>>> GetActiveWallets(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<WalletViewModel>>> GetInActiveWallets(int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<Pagination<WalletViewModel>>> GetWalletsByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10);
        Task<ResponseView<WalletViewModel>> GetWalletById(Guid walletId);
        Task<ResponseView<WalletViewModel>> CreateWallet(CreateWalletViewModel model);
        Task<ResponseView<WalletViewModel>> UpdateWallet(Guid walletId, CreateWalletViewModel walletDTO);
        Task<ResponseView<WalletViewModel>> DeleteWallet(Guid walletId);
        Task<ResponseView<WalletPermission>> ApproveWallet(Guid walletId);
        Task<ResponseView<WalletPermission>> RejectWallet(Guid walletId);
    }
}
