﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Application.ViewModels.CardViewModels;
using Application.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Application.Services
{
    public class CardService : ICardService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CardService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<CardViewModel>> CreateCard(CreateCardViewModel model)
        {
            var checkId = await _unitOfWork.CardTypeRepository.GetEntityByIdAsync(model.CardTypeId);
            if (checkId != null)
            {
                var card = _mapper.Map<Card>(model);
                card.ExpirationDate = DateTime.Now.AddYears(4);
                await _unitOfWork.CardRepository.AddEntityAsync(card);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<CardViewModel>(card);
                    return new ResponseViewSuccess<CardViewModel>(result);
                }
                return new ResponseViewError<CardViewModel>("Create Fail, Invalid Input Information");
            }
            return new ResponseViewError<CardViewModel>("Invalid CardType Id, Please Check Id Again !");
        }

        public async Task<ResponseView<CardViewModel>> DeleteCard(Guid cardId)
        {
            var card = await _unitOfWork.CardRepository.GetEntityByIdAsync(cardId);
            if (card is not null)
            {
                _unitOfWork.CardRepository.SoftRemoveEntity(card);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<CardViewModel>(card);
                    return new ResponseViewSuccess<CardViewModel>(result);
                }
            }
            return new ResponseViewError<CardViewModel>("Invalid Id, Please Check Id Again !");
        }

        public async Task<ResponseView<Pagination<CardViewModel>>> GetCurrentCard(int pageIndex = 0, int pageSize = 10)
        {
            var card = await _unitOfWork.CardRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<CardViewModel>>(card);
            if (card.Items.Count < 1) return new ResponseViewError<Pagination<CardViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<CardViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<CardViewModel>>> GetHistoryCard(int pageIndex = 0, int pageSize = 10)
        {
            var card = await _unitOfWork.CardRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<CardViewModel>>(card);
            if (card.Items.Count < 1) return new ResponseViewError<Pagination<CardViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<CardViewModel>>(result);
        }

        public async Task<ResponseView<CardViewModel>> GetCardById(Guid cardId)
        {
            var card = await _unitOfWork.CardRepository.FirstOrDefaultAsync(x => x.Id == cardId);
            var result = _mapper.Map<CardViewModel>(card);
            if (card is null) return new ResponseViewError<CardViewModel>("Not Found");
            else return new ResponseViewSuccess<CardViewModel>(result);
        }

        public async Task<ResponseView<Pagination<CardViewModel>>> GetCardsByCardTypeId(Guid cardTypeId, int pageIndex = 0, int pageSize = 10)
        {
            var card = await _unitOfWork.CardRepository.ToPaginationNoQueryFilter(expression: x => x.CardTypeId == cardTypeId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<CardViewModel>>(card);
            if (card is null) return new ResponseViewError<Pagination<CardViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<CardViewModel>>(result);
        }

        public async Task<ResponseView<CardViewModel>> UpdateCard(Guid cardId, UpdateCardViewModel request)
        {
            var card = await _unitOfWork.CardRepository.FirstOrDefaultAsync(x => x.Id == cardId);
            if (card is not null)
            {
                _mapper.Map(request, card);
                _unitOfWork.CardRepository.UpdateEntity(card);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<CardViewModel>(card);
                    return new ResponseViewSuccess<CardViewModel>(result);
                }
            }
            return new ResponseViewError<CardViewModel>("Invalid Id, Please Check Id Again !");
        }
    }
}
