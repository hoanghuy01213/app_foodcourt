﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.CardTypeViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopsViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Application.Services
{
    public class CardTypeService : ICardTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CardTypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<CardTypeViewModel>> CreateCardType(CreateCardTypeViewModel cardTypeViewModel)
        {
            var model = _mapper.Map<CardType>(cardTypeViewModel);
            await _unitOfWork.CardTypeRepository.AddEntityAsync(model);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            if(isSuccess)
            {
                var result =  _mapper.Map<CardTypeViewModel>(model);
                return new ResponseViewSuccess<CardTypeViewModel>(result);
            }
            return new ResponseViewError<CardTypeViewModel>("Create Fail, Invalid Input Information");
        }

        public async Task<ResponseView<CardTypeViewModel>> DeleteCardType(Guid cardTypeId)
        {
            var cardType = await _unitOfWork.CardTypeRepository.GetEntityByIdAsync(cardTypeId);
            if(cardType is not null)
            {
                _unitOfWork.CardTypeRepository.SoftRemoveEntity(cardType);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if(isSuccess)
                {
                    var result = _mapper.Map<CardTypeViewModel>(cardType);
                    return new ResponseViewSuccess<CardTypeViewModel>(result);
                }
            }
            return new ResponseViewError<CardTypeViewModel>("Invalid Id, Please Check Id Again !");
        }

        public async Task<ResponseView<Pagination<CardTypeViewModel>>> GetCurrentCardType(int pageIndex = 0, int pageSize = 10)
        {
            var cardType = await _unitOfWork.CardTypeRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<CardTypeViewModel>>(cardType);
            if (cardType is null) return new ResponseViewError<Pagination<CardTypeViewModel>>("Not Found");
            return new ResponseViewSuccess<Pagination<CardTypeViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<CardTypeViewModel>>> GetHistoryCardType(int pageIndex = 0, int pageSize = 10)
        {
            var cardType = await _unitOfWork.CardTypeRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<CardTypeViewModel>>(cardType);
            if (cardType is null) return new ResponseViewError<Pagination<CardTypeViewModel>>("Not Found");
            return new ResponseViewSuccess<Pagination<CardTypeViewModel>>(result);
        }
        public async Task<ResponseView<CardTypeViewModel>> GetCardTypeById(Guid cardTypeId)
        {
            var cardType = await _unitOfWork.CardTypeRepository.FirstOrDefaultAsync(x=>x.Id==cardTypeId);
            var result = _mapper.Map<CardTypeViewModel>(cardType);
            if (cardType is null) return new ResponseViewError<CardTypeViewModel>("Not Found");
            return new ResponseViewSuccess<CardTypeViewModel>(result);
        }

        public async Task<ResponseView<CardTypeViewModel>> UpdateCardType(Guid cardTypeId, CreateCardTypeViewModel cardType)
        {
            var model = await _unitOfWork.CardTypeRepository.GetEntityByIdAsync(cardTypeId);
            if(model is not null)
            {
                _mapper.Map(cardType, model);
                _unitOfWork.CardTypeRepository.UpdateEntity(model);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if(isSuccess)
                {
                    var result = _mapper.Map<CardTypeViewModel>(model);
                    return new ResponseViewSuccess<CardTypeViewModel>(result);
                }
            }
            return new ResponseViewError<CardTypeViewModel>("Invalid Id, Please Check Id Again !");
        }
    }
}
