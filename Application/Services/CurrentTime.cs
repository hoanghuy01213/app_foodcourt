﻿using Application.Interfaces;

namespace Application.Services
{
    public class CurrentTime : ICurrentTimeService
    {
        public DateTime GetCurrentTime() => DateTime.UtcNow;
    }
}
