﻿using Application.Commons;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Application.Services
{
    public class JwtService : IJwtService
    {
        private readonly AppConfiguration _configuration;
        public JwtService(AppConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GenerateJsonWebToken(User user)
        {
            var issuer = _configuration.JWT.Issuer;
            var audience = _configuration.JWT.Audience;
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.JWT.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim("Email",user.Email),
                new Claim("UserName", user.UserName),
                new Claim(ClaimTypes.Role,user.Role.ToString()),
                new Claim("Role",user.Role.ToString())
            };
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                audience: audience,
                issuer: issuer,
                signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
