﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;

        public OrderDetailService(IUnitOfWork unitOfWork, IMapper mapper, IOrderService orderService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _orderService = orderService;
        }
        public async Task<ResponseView<Pagination<OrderResponseViewModel>>> GetOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var listOrderDetail = await _unitOfWork.OrderRepository.ToPagination(
                expression: x => x.Id == orderId,
                include: x => x.Include(x => x.OrderDetails),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.OrderDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderResponseViewModel>>(listOrderDetail);
            if (listOrderDetail is null) return new ResponseViewError<Pagination<OrderResponseViewModel>>("Can't get OrderDetail");
            else return new ResponseViewSuccess<Pagination<OrderResponseViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<OrderResponseViewModel>>> GetHistoryOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var listOrderDetail = await _unitOfWork.OrderRepository.ToPaginationNoQueryFilter(
                expression: x => x.Id == orderId,
                include: x => x.Include(x => x.OrderDetails),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.OrderDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderResponseViewModel>>(listOrderDetail);
            if (listOrderDetail is null) return new ResponseViewError<Pagination<OrderResponseViewModel>>("Can't get OrderDetail");
            else return new ResponseViewSuccess<Pagination<OrderResponseViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<OrderResponseViewModel>>> GetCurrentOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var listOrderDetail = await _unitOfWork.OrderRepository.ToPagination(
                expression: x => x.Id == orderId,
                include: x => x.Include(x => x.OrderDetails),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.OrderDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderResponseViewModel>>(listOrderDetail);
            if (listOrderDetail is null) return new ResponseViewError<Pagination<OrderResponseViewModel>>("Can't get OrderDetail");
            else return new ResponseViewSuccess<Pagination<OrderResponseViewModel>>(result);
        }

        public async Task<ResponseView<OrderResponseViewModel>> AddOrder(Guid Id, CreateOrderDetailViewModel request)
        {
            var orderDetail = _mapper.Map<OrderDetail>(request);
            var order = await _unitOfWork.OrderRepository.GetEntityByGenericAsync(
                expression: x => x.Id == Id,
                include: x => x.Include(x => x.OrderDetails)
            );
            if (order is null)
                return new ResponseViewError<OrderResponseViewModel>("Not found the order");

            var existOrderDetail = await _unitOfWork.OrderDetailRepository.GetEntityByGenericAsync(x => x.ProductId == request.ProductId);
            _unitOfWork.BeginTransaction();
            if (existOrderDetail is not null)
            {
                existOrderDetail.Quantity += request.Quantity;
                _unitOfWork.OrderDetailRepository.UpdateEntity(existOrderDetail);
                order.TotalAmount = await _orderService.CalculateTotalPriceAsync(await _unitOfWork.OrderDetailRepository.GetAsync(x => x.Id != null));
            }
            else
            {
                order.OrderDate = DateTime.Now;
                await _unitOfWork.OrderDetailRepository.AddEntityAsync(orderDetail);
                order.TotalAmount = await _orderService.CalculateTotalPriceAsync(order.OrderDetails);
            }
            try
            {
                await _unitOfWork.CommitAsync();
                var result = _mapper.Map<OrderResponseViewModel>(order);
                return new ResponseViewSuccess<OrderResponseViewModel>(result);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return new ResponseViewError<OrderResponseViewModel>("Can't add order", new List<string> { ex.ToString() });
            }
        }
        public async Task<ResponseView<OrderResponseViewModel>> UpdateOrderDetail(Guid Id, UpdateOrderDetailViewModel request)
        {
            var orderDetail = _mapper.Map<OrderDetail>(request);

            var order = await _unitOfWork.OrderRepository.GetEntityByGenericAsync(
                expression: x => x.Id == Id,
                include: x => x.Include(x => x.OrderDetails));

            var updateOrderDetail = order.OrderDetails.FirstOrDefault(x => x.OrderId == order.Id && x.ProductId == request.ProductId);

            if (updateOrderDetail is null)
                return new ResponseViewError<OrderResponseViewModel>("Can not found order details");
            updateOrderDetail.Quantity = orderDetail.Quantity;
            order.OrderDate = DateTime.Now;
            order.TotalAmount = await _orderService.CalculateTotalPriceAsync(order.OrderDetails);
            try
            {
                await _unitOfWork.ExecuteTransactionAsync(() => _unitOfWork.OrderRepository.UpdateEntity(order));

                var result = _mapper.Map<OrderResponseViewModel>(order);

                return new ResponseViewSuccess<OrderResponseViewModel>(result);
            }
            catch (Exception ex)
            {
                return new ResponseViewError<OrderResponseViewModel>("Can't update order detail", new List<string> { ex.ToString() });
            }
        }
        public async Task<ResponseView<OrderDetails>> DeleteOrderDetail(Guid orderId, Guid productId)
        {
            var orderDetail = await _unitOfWork.OrderDetailRepository.GetEntityByGenericAsync(x => x.OrderId == orderId && x.ProductId == productId);
            if (orderDetail is null)
                return new ResponseViewError<OrderDetails>("Order detail not found");

            try
            {
                await _unitOfWork.ExecuteTransactionAsync(() => _unitOfWork.OrderDetailRepository.SoftRemoveEntity(orderDetail));
                var result = _mapper.Map<OrderDetails>(orderDetail);

                return new ResponseViewSuccess<OrderDetails>(result);
            }
            catch (Exception ex)
            {
                return new ResponseViewError<OrderDetails>("Can't delete order", new List<string> { ex.ToString() });
            }
        }
    }
}
