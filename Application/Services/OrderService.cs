﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.ShopViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<ApproveOrderViewModel>> ApproveOrder(Guid orderId)
        {
            var order = await _unitOfWork.OrderRepository.GetEntityByGenericAsync(x => x.Id == orderId);
            if (order is null)
            {
                return new ResponseViewError<ApproveOrderViewModel>("Not found Order!!!");
            }
            order.OrderStatus = OrderStatus.Success;
            var newOrder = _mapper.Map<Order>(order);
            _unitOfWork.OrderRepository.UpdateEntity(order);
            var result = _mapper.Map<ApproveOrderViewModel>(newOrder);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<ApproveOrderViewModel>(result) : new ResponseViewError<ApproveOrderViewModel>("Approve Order Fail!!!");
        }
        public async Task<ResponseView<ApproveOrderViewModel>> RejectOrder(Guid orderId)
        {
            var order = await _unitOfWork.OrderRepository.GetEntityByGenericAsync(x => x.Id == orderId);
            if (order is null)
            {
                return new ResponseViewError<ApproveOrderViewModel>("Not found Order!!!");
            }
            order.OrderStatus = OrderStatus.Fail;
            var newOrder = _mapper.Map<Order>(order);
            _unitOfWork.OrderRepository.UpdateEntity(order);
            var result = _mapper.Map<ApproveOrderViewModel>(newOrder);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<ApproveOrderViewModel>(result) : new ResponseViewError<ApproveOrderViewModel>("Reject Order Fail!!!");
        }
        public async Task<decimal> CalculateTotalPriceAsync(IEnumerable<OrderDetail> orderDetails)
        {
            decimal totalPrice = 0;
            foreach (var item in orderDetails)
            {
                var product = await _unitOfWork.ProductRepository.GetEntityByGenericAsync(x => x.Id == item.ProductId);
                /// if product is not found for this ProductId
                if (product is null)
                {
                    throw new Exception($"Product not found for ProductId: {item.ProductId}");
                }
                decimal itemPrice = product.Price * item.Quantity;
                totalPrice += itemPrice;
            }
            return totalPrice;
        }
        public async Task<ResponseView<OrderResponseViewModel>> CreateOrder(CreateOrderViewModel request)
        {
            var checkId = await _unitOfWork.CardRepository.GetEntityByIdAsync(request.CardId);
            if (checkId != null)
            {
                var order = _mapper.Map<Order>(request);
                order.OrderDate = DateTime.Now;
                order.OrderStatus = OrderStatus.Pending;
                order.TotalAmount = await CalculateTotalPriceAsync(order.OrderDetails);
                try
                {
                    await _unitOfWork.ExecuteTransactionAsync(() => _unitOfWork.OrderRepository.AddEntityAsync(order));
                    var result = _mapper.Map<OrderResponseViewModel>(order);
                    return new ResponseViewSuccess<OrderResponseViewModel>(result);
                }
                catch (Exception ex)
                {
                    _unitOfWork.Rollback();
                    return new ResponseViewError<OrderResponseViewModel>(
                                        "Can't add order",
                                        new List<string> { ex.ToString() });
                }
            }
            return new ResponseViewError<OrderResponseViewModel>("Invalid Card Id, Please Check Id Again !");
        }

        public async Task<ResponseView<OrderResponseViewModel>> Update(Guid orderId, UpdateOrderViewModel request)
        {
            var order = await _unitOfWork.OrderRepository.GetEntityByGenericAsync(x => x.Id == orderId);
            var checkId = await _unitOfWork.CardRepository.GetEntityByIdAsync(request.CardId);
            if (checkId is null) return new ResponseViewError<OrderResponseViewModel>("Invalid Card Id, Please Check Id Again !");
            /// Order is not found.
            if (order is null)
            {
                return new ResponseViewError<OrderResponseViewModel>("Order not found");
            }
            var newOrder = _mapper.Map(request, order);
            try
            {
                await _unitOfWork.ExecuteTransactionAsync(() =>
                {
                    _unitOfWork.OrderRepository.UpdateEntity(order);
                });
                var result = _mapper.Map<OrderResponseViewModel>(newOrder);
                return new ResponseViewSuccess<OrderResponseViewModel>(result);
            }
            catch (Exception ex)
            {
                return new ResponseViewError<OrderResponseViewModel>(
                    "Can't update order",
                    new List<string> { ex.ToString() });
            }
        }
        public async Task<ResponseView<OrderViewModel>> DeleteOrder(Guid orderId)
        {
            var order = await _unitOfWork.OrderRepository.GetEntityByGenericAsync(x => x.Id == orderId);
            if (order is null)
            {
                return new ResponseViewError<OrderViewModel>("Order not found");
            }
            try
            {
                await _unitOfWork.ExecuteTransactionAsync(() => _unitOfWork.OrderRepository.SoftRemoveEntity(order));
                var result = _mapper.Map<OrderViewModel>(order);
                return new ResponseViewSuccess<OrderViewModel>(result);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return new ResponseViewError<OrderViewModel>(
                                    "Can't delete order",
                                    new List<string> { ex.ToString() });
            }
        }

        public async Task<ResponseView<Pagination<OrderViewModel>>> GetCurrentOrder(int pageIndex = 0, int pageSize = 10)
        {
            var order = await _unitOfWork.OrderRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.OrderDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderViewModel>>(order);
            if (order is null) return new ResponseViewError<Pagination<OrderViewModel>>("Can't get Order");
            else return new ResponseViewSuccess<Pagination<OrderViewModel>>(result);
        }
        public async Task<ResponseView<Pagination<OrderViewModel>>> GetHistoryOrder(int pageIndex = 0, int pageSize = 10)
        {
            var order = await _unitOfWork.OrderRepository.ToPaginationNoQueryFilter(expression: x => x.OrderDate < DateTime.Now, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.OrderDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderViewModel>>(order);
            if (order is null) return new ResponseViewError<Pagination<OrderViewModel>>("Can't get Order ");
            else return new ResponseViewSuccess<Pagination<OrderViewModel>>(result);
        }
        public async Task<ResponseView<OrderViewModel>> GetOrderById(Guid orderId)
        {
            var order = await _unitOfWork.OrderRepository.FirstOrDefaultAsync(x => x.Id == orderId);
            var result = _mapper.Map<OrderViewModel>(order);
            if (order is null) return new ResponseViewError<OrderViewModel>("Can't get Order by Id");
            else return new ResponseViewSuccess<OrderViewModel>(result);
        }

        public async Task<ResponseView<Pagination<OrderViewModel>>> GetOrdersByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10)
        {
            var order = await _unitOfWork.OrderRepository.ToPaginationNoQueryFilter(expression: x => x.CardId == cardId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.OrderDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderViewModel>>(order);
            if (order is null) return new ResponseViewError<Pagination<OrderViewModel>>("Can't get Order by CardId");
            else return new ResponseViewSuccess<Pagination<OrderViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopViewIncludeModel>>> GetOrdersByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPagination
                (expression: x => x.Id == shopId,
                include: x => x
                .Include(x => x.ShopCategories)
                .ThenInclude(x => x.Products)
                .ThenInclude(x => x.OrderDetails)
                .ThenInclude(x => x.Orders),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewIncludeModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewIncludeModel>>("Can't get Order by ShopId");
            else return new ResponseViewSuccess<Pagination<ShopViewIncludeModel>>(result);
        }
        public async Task<ResponseView<Pagination<UserViewIncludeModel>>> GetOrdersByUserId(Guid userId, int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.UserRepository.ToPagination
                (expression: x => x.Id == userId,
                include: x => x
                .Include(x=>x.Shops)
                .ThenInclude(x=>x.ShopCategories)
                .ThenInclude(x => x.Products)
                .ThenInclude(x => x.OrderDetails)
                .ThenInclude(x => x.Orders),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<UserViewIncludeModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<UserViewIncludeModel>>("Can't get Order by ShopId");
            else return new ResponseViewSuccess<Pagination<UserViewIncludeModel>>(result);
        }
        public async Task<ResponseView<Pagination<OrderViewModel>>> GetPendingOrders(int pageIndex = 0, int pageSize = 10)
        {
            var order = await _unitOfWork.OrderRepository.ToPagination(expression: x => x.OrderStatus == OrderStatus.Pending, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.OrderDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderViewModel>>(order);
            if (order is null) return new ResponseViewError<Pagination<OrderViewModel>>("Can't get Pending Order");
            else return new ResponseViewSuccess<Pagination<OrderViewModel>>(result);
        }
        public async Task<ResponseView<Pagination<OrderViewModel>>> GetFailOrders(int pageIndex = 0, int pageSize = 10)
        {
            var order = await _unitOfWork.OrderRepository.ToPagination(expression: x => x.OrderStatus == OrderStatus.Fail, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.OrderDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderViewModel>>(order);
            if (order is null) return new ResponseViewError<Pagination<OrderViewModel>>("Can't get Fail Order");
            else return new ResponseViewSuccess<Pagination<OrderViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<OrderViewModel>>> GetSuccessOrders(int pageIndex = 0, int pageSize = 10)
        {
            var order = await _unitOfWork.OrderRepository.ToPagination(expression: x => x.OrderStatus == OrderStatus.Success, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.OrderDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<OrderViewModel>>(order);
            if (order is null) return new ResponseViewError<Pagination<OrderViewModel>>("Can't get Sucess Order");
            else return new ResponseViewSuccess<Pagination<OrderViewModel>>(result);
        }

    }
}
