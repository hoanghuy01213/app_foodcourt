﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Xml.Linq;

namespace Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<ProductViewModel>> CreateProduct(CreateProductViewModel model)
        {
            var checkId = await _unitOfWork.ShopCategoryRepository.GetEntityByIdAsync(model.ShopCategoryId);
            if (checkId == null) return new ResponseViewError<ProductViewModel>("Invalid ShopCategory Id, Please Check Id Again !");
            var product = _mapper.Map<Product>(model);
            await _unitOfWork.ProductRepository.AddEntityAsync(product);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            if (isSuccess)
            {
                var result = _mapper.Map<ProductViewModel>(product);
                return new ResponseViewSuccess<ProductViewModel>(result);
            }
            return new ResponseViewError<ProductViewModel>("Create Fail, Invalid Input Information");
        }

        public async Task<ResponseView<Pagination<ProductViewModel>>> GetCurrentProduct(int pageIndex = 0, int pageSize = 10)
        {
            var product = await _unitOfWork.ProductRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ProductViewModel>>(product);
            if (product is null) return new ResponseViewError<Pagination<ProductViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ProductViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ProductViewModel>>> GetHistoryProduct(int pageIndex = 0, int pageSize = 10)
        {
            var product = await _unitOfWork.ProductRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ProductViewModel>>(product);
            if (product is null) return new ResponseViewError<Pagination<ProductViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ProductViewModel>>(result);
        }

        public async Task<ResponseView<ProductViewModel>> GetProductById(Guid productId)
        {
            var product = await _unitOfWork.ProductRepository.FirstOrDefaultAsync(x => x.Id == productId);
            var result = _mapper.Map<ProductViewModel>(product);
            if (product is null) return new ResponseViewError<ProductViewModel>("Not Found");
            else return new ResponseViewSuccess<ProductViewModel>(result);
        }

        public async Task<ResponseView<Pagination<ShopViewProductViewModel>>> GetProductByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPagination(
                expression: x => x.Id == shopId,
                include: x => x.Include(a => a.ShopCategories)
                .ThenInclude(b => b.Products),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewProductViewModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewProductViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopViewProductViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ProductViewModel>>> GetProductsByName(string productName, int pageIndex = 0, int pageSize = 10)
        {
            var product = await _unitOfWork.ProductRepository.ToPagination(expression: x => x.Name == productName, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ProductViewModel>>(product);
            if (product is null) return new ResponseViewError<Pagination<ProductViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ProductViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ProductViewModel>>> GetProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10)
        {
            var product = await _unitOfWork.ProductRepository.ToPagination(expression: x => x.ShopCategoryId == shopCategoryId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ProductViewModel>>(product);
            if (product is null) return new ResponseViewError<Pagination<ProductViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ProductViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ProductViewModel>>> GetHistoryProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10)
        {
            var product = await _unitOfWork.ProductRepository.ToPaginationNoQueryFilter(expression: x => x.ShopCategoryId == shopCategoryId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ProductViewModel>>(product);
            if (product is null) return new ResponseViewError<Pagination<ProductViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ProductViewModel>>(result);
        }

        public async Task<ResponseView<ProductViewModel>> RemoveProduct(Guid productId)
        {
            var shop = await _unitOfWork.ProductRepository.GetEntityByIdAsync(productId);
            if (shop is not null)
            {
                _unitOfWork.ProductRepository.SoftRemoveEntity(shop);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<ProductViewModel>(shop);
                    return new ResponseViewSuccess<ProductViewModel>(result);
                }
            }
            return new ResponseViewError<ProductViewModel>("Invalid Product Id, Please Check Id Again !");
        }

        public async Task<ResponseView<ProductViewModel>> UpdateProduct(Guid productId, CreateProductViewModel model)
        {
            var checkId = await _unitOfWork.ShopCategoryRepository.GetEntityByIdAsync(model.ShopCategoryId);
            if (checkId != null)
            {
                var shop = await _unitOfWork.ProductRepository.GetEntityByIdAsync(productId);
                if (shop != null)
                {
                    _mapper.Map(model, shop);
                    _unitOfWork.ProductRepository.UpdateEntity(shop);
                    var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                    if (isSuccess)
                    {
                        var result = _mapper.Map<ProductViewModel>(shop);
                        return new ResponseViewSuccess<ProductViewModel>(result);
                    }
                }
                return new ResponseViewError<ProductViewModel>("Invalid Product Id, Please Check Again !");
            }
            return new ResponseViewError<ProductViewModel>("Invalid ShopCategory Id, Please Check Id Again !");
        }

        public async Task<ResponseView<Pagination<TopSellingProduct>>> GetTopSellingProducts(
            DateTime start,
            DateTime end,
            int pageIndex = 0,
            int pageSize = 10)
        {
            var orderDetails = await _unitOfWork.OrderDetailRepository.ToPagination(
                expression: od => od.Orders.OrderDate >= start && od.Orders.OrderDate <= end,
                include: od => od.Include(od => od.Products),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: od => od.Quantity,
                sortDirection: SortDirection.Descending);

            var products = orderDetails.Items.GroupBy(od => od.Products)
                .Select(group => new
                {
                    Product = group.Key,
                    TotalQuantity = group.Sum(od => od.Quantity)
                })
                .OrderByDescending(group => group.TotalQuantity)
                .Skip(pageIndex * pageSize)
                .Take(pageSize)
                .Select(group => new TopSellingProduct
                {
                    ProductId = group.Product.Id,
                    ProductName = group.Product.Name,
                    TotalQuantity = group.TotalQuantity
                })
                .ToList();

            if (products == null)
                return new ResponseViewError<Pagination<TopSellingProduct>>("Can't get product");

            var pagination = new Pagination<TopSellingProduct>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = orderDetails.TotalItemCount,
                Items = products
            };
            return new ResponseViewSuccess<Pagination<TopSellingProduct>>(pagination);
        }
    }
}
