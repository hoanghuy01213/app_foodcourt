﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.Response;
using Application.ViewModels.SaleReportViewmodels;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SaleReportService : ISaleReportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        public SaleReportService(IUnitOfWork unitOfWork, IOrderService orderService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _orderService = orderService;
            _mapper = mapper;
        }

        public async Task<ResponseView<SaleReportViewModel>> GetSalesReport(DateTime startDate, DateTime endDate)
        {
            var orders = await _unitOfWork.OrderRepository.ToPagination(
                expression: o => o.OrderDate >= startDate && o.OrderDate <= endDate,
                include: o => o.Include(o => o.OrderDetails),
                pageIndex: 0,
                pageSize: int.MaxValue);

            var orderDetail = orders.Items.SelectMany(x => x.OrderDetails);
            var total = await _orderService.CalculateTotalPriceAsync(orderDetail);


            var salesByDay = orders.Items.GroupBy(
                    o =>
                    new { Year = o.OrderDate.Year, Month = o.OrderDate.Month, Day = o.OrderDate.Day })
                .Select(g => new { Date = g.Key, TotalSales = total })
                .OrderBy(g => g.Date.Year)
                .ThenBy(g => g.Date.Month)
                .ThenBy(g => g.Date.Day)
                .ToList();

            var totalSales = salesByDay.Sum(s => s.TotalSales);

            var salesReport = new SaleReportViewModel
            {
                StartDate = startDate,
                EndDate = endDate,
                TotalSales = totalSales,
                DailySales = salesByDay.Select(s => new DailySalesViewModel
                {
                    Date = new DateTime(s.Date.Year, s.Date.Month, s.Date.Day),
                    TotalSales = s.TotalSales
                }).ToList()
            };
            return new ResponseViewSuccess<SaleReportViewModel>(salesReport);
        }

        public async Task<ResponseView<SaleReportViewModel>> GetSalesReport(int year, int month)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var result = await GetSalesReport(startDate, endDate);
            return new ResponseViewSuccess<SaleReportViewModel>(result.Result);
        }

        public async Task<ResponseView<List<SaleReportViewModel>>> GetSalesReportByEachMonth()
        {
            var list = new List<SaleReportViewModel>();
            var endDate=DateTime.Now.AddDays(-1);
            var startDate = DateTime.Now.AddMonths(-12).AddDays(1);
            for (int i = 0; i < 12; i++)
            {
                endDate = startDate.AddMonths(1).AddDays(-1);
                var result = await GetSalesReport(startDate, endDate);
                list.Add(result.Result);
                startDate = startDate.AddMonths(1);
            }
            return new ResponseViewSuccess<List<SaleReportViewModel>>(list);
        }
    }
}
