﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.Response;
using Application.ViewModels.ShopCategoryViewModels;
using Application.ViewModels.ShopsViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;
using System.Xml.Linq;

namespace Application.Services
{
    public class ShopCategoryService : IShopCategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ShopCategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<bool> CheckShopId(Guid shopId) => await _unitOfWork.ShopCategoryRepository.CheckExistGeneric(x => x.ShopId == shopId);
        public async Task<ResponseView<ShopCategoryViewModel>> CreateShopCategory(CreateShopCategoryViewModel shopCateDTO)
        {
            var checkId = await _unitOfWork.ShopRepository.GetEntityByIdAsync(shopCateDTO.ShopId);
            if (checkId != null)
            {
                var shopCateMap = _mapper.Map<ShopCategory>(shopCateDTO);
                await _unitOfWork.ShopCategoryRepository.AddEntityAsync(shopCateMap);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<ShopCategoryViewModel>(shopCateMap);
                    return new ResponseViewSuccess<ShopCategoryViewModel>(result);
                }
                return new ResponseViewError<ShopCategoryViewModel>("Create Fail, Invalid Input Information");
            }
            return new ResponseViewError<ShopCategoryViewModel>("Invalid Shop Id, Please Check Id Again");
        }

        public async Task<ResponseView<ShopCategoryViewModel>> DeleteShopCategory(Guid shopCateId)
        {
            var shopCateObj = await _unitOfWork.ShopCategoryRepository.GetEntityByIdAsync(shopCateId);
            if (shopCateObj is not null)
            {
                _unitOfWork.ShopCategoryRepository.SoftRemoveEntity(shopCateObj);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<ShopCategoryViewModel>(shopCateObj);
                    return new ResponseViewSuccess<ShopCategoryViewModel>(result);
                }
            }
            return new ResponseViewError<ShopCategoryViewModel>("Invalid Id, Please Check Id Again !");
        }

        public async Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetCurrentShopCategory(int pageIndex = 0, int pageSize = 10)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopCategoryViewModel>>(shopCate);
            if (shopCate is null) return new ResponseViewError<Pagination<ShopCategoryViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopCategoryViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetHistoryShopCategory(int pageIndex = 0, int pageSize = 10)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopCategoryViewModel>>(shopCate);
            if (shopCate is null) return new ResponseViewError<Pagination<ShopCategoryViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopCategoryViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetShopCategoriesByName(string name, int pageIndex = 0, int pageSize = 10)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.ToPagination(expression: x => x.Name == name, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopCategoryViewModel>>(shopCate);
            if (shopCate is null) return new ResponseViewError<Pagination<ShopCategoryViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopCategoryViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetShopCategoriesByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.ToPagination(expression: x => x.ShopId == shopId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopCategoryViewModel>>(shopCate);
            if (shopCate is null) return new ResponseViewError<Pagination<ShopCategoryViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopCategoryViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopCategoryViewModel>>> GetHistoryShopCategoriesByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.ToPaginationNoQueryFilter(expression: x => x.ShopId == shopId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopCategoryViewModel>>(shopCate);
            if (shopCate is null) return new ResponseViewError<Pagination<ShopCategoryViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopCategoryViewModel>>(result);
        }

        public async Task<ResponseView<ShopCategoryViewModel>> GetShopCategoryById(Guid shopCateId)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.FirstOrDefaultAsync(x => x.Id == shopCateId);
            var result = _mapper.Map<ShopCategoryViewModel>(shopCate);
            if (shopCate is null) return new ResponseViewError<ShopCategoryViewModel>("Not Found");
            else return new ResponseViewSuccess<ShopCategoryViewModel>(result);
        }

        public async Task<ResponseView<ShopCategoryViewModel>> UpdateShopCategory(Guid shopCateId, CreateShopCategoryViewModel shopCateDTO)
        {
            var shopCate = await _unitOfWork.ShopCategoryRepository.GetEntityByIdAsync(shopCateId);
            if (shopCate is not null)
            {
                var checkId = await _unitOfWork.ShopRepository.GetEntityByIdAsync(shopCateDTO.ShopId);
                if (checkId != null)
                {
                    _mapper.Map(shopCateDTO, shopCate);
                    _unitOfWork.ShopCategoryRepository.UpdateEntity(shopCate);
                    var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                    if (isSuccess)
                    {
                        var result = _mapper.Map<ShopCategoryViewModel>(shopCate);
                        return new ResponseViewSuccess<ShopCategoryViewModel>(result);
                    }
                }
                return new ResponseViewError<ShopCategoryViewModel>("Invalid Shop Id, Please Check Id Again");
            }
            return new ResponseViewError<ShopCategoryViewModel>("Invalid ShopCategory Id, Please Check Id Again");
        }
    }
}
