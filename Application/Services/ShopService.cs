﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Application.Services
{
    public class ShopService : IShopService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ShopService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<ShopViewModel>> CreateShop(CreateShopViewModel shopDTO)
        {
            var checkId = await _unitOfWork.UserRepository.GetEntityByIdAsync(shopDTO.UserId);
            if (checkId != null)
            {
                var shop = _mapper.Map<Shop>(shopDTO);
                await _unitOfWork.ShopRepository.AddEntityAsync(shop);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<ShopViewModel>(shop);
                    return new ResponseViewSuccess<ShopViewModel>(result);
                }
                return new ResponseViewError<ShopViewModel>("Create Fail, Invalid Input Information");
            }
            return new ResponseViewError<ShopViewModel>("Invalid User Id, Please Check Id Again !");
        }

        public async Task<ResponseView<ShopViewModel>> DeleteShop(Guid shopId)
        {
            var shopObj = await _unitOfWork.ShopRepository.GetEntityByIdAsync(shopId);
            if (shopObj is not null)
            {
                _unitOfWork.ShopRepository.SoftRemoveEntity(shopObj);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<ShopViewModel>(shopObj);
                    return new ResponseViewSuccess<ShopViewModel>(result);
                }
            }
            return new ResponseViewError<ShopViewModel>("Invalid Shop Id, Please Check Id Again !");
        }
        public async Task<ResponseView<Pagination<ShopViewModel>>> GetCurrentShop(int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewModel>>("Not Found Shop!!!");
            else return new ResponseViewSuccess<Pagination<ShopViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopViewModel>>> GetHistoryShop(int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewModel>>("Not Found Shop!!!");
            else return new ResponseViewSuccess<Pagination<ShopViewModel>>(result);
        }
        public async Task<ResponseView<ShopViewModel>> GetShopById(Guid shopId)
        {
            var shop = await _unitOfWork.ShopRepository.FirstOrDefaultAsync(x => x.Id == shopId);
            var result = _mapper.Map<ShopViewModel>(shop);
            if (shop is null) return new ResponseViewError<ShopViewModel>("Not Found Shop by Id!!!");
            else return new ResponseViewSuccess<ShopViewModel>(result);
        }

        public async Task<ResponseView<Pagination<ShopResponseViewModel>>> GetShopDetails(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var listShopDetail = await _unitOfWork.ShopRepository.ToPagination(
                expression: x => x.Id == shopId,
                include: x => x.Include(a => a.ShopCategories),
                pageIndex: pageIndex, pageSize: pageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopResponseViewModel>>(listShopDetail);
            if (listShopDetail is null) return new ResponseViewError<Pagination<ShopResponseViewModel>>("Not Found Shop!!!");
            else return new ResponseViewSuccess<Pagination<ShopResponseViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopViewModel>>> GetShopByName(string name, int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPagination(expression: x => x.ShopName == name, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<ShopViewModel>>> GetShopByUserId(Guid userId, int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPagination(expression: x => x.UserId == userId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<ShopViewModel>>(result);
        }

        public async Task<ResponseView<ShopViewModel>> UpdateShop(Guid shopId, UpdateShopViewModel shopDTO)
        {
            var shop = await _unitOfWork.ShopRepository.GetEntityByIdAsync(shopId);
            if (shop is not null)
            {
                _mapper.Map(shopDTO, shop);
                _unitOfWork.ShopRepository.UpdateEntity(shop);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<ShopViewModel>(shop);
                    return new ResponseViewSuccess<ShopViewModel>(result);
                }
            }
            return new ResponseViewError<ShopViewModel>("Invalid Shop Id, Please Check Id Again !");
        }
    }
}
