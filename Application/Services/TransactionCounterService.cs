﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.Response;
using Application.ViewModels.TransactionCounterViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Application.Services
{
    public class TransactionCounterService : ITransactionCounterService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TransactionCounterService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<TransactionCounterViewModel>> CreateTransactionCounter(CreateTransactionCounterViewModel transactionCounter)
        {
            var tranCount = _mapper.Map<TransactionCounter>(transactionCounter);
            await _unitOfWork.TransactionCounterRepository.AddEntityAsync(tranCount);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            if(isSuccess)
            {
                var result =  _mapper.Map<TransactionCounterViewModel>(tranCount);
                return new ResponseViewSuccess<TransactionCounterViewModel>(result);
            }
            return new ResponseViewError<TransactionCounterViewModel>("Create Fail, Invalid Input Information");
        }

        public async Task<ResponseView<TransactionCounterViewModel>> DeleteTransactionCounter(Guid transactionCounterId)
        {
            var tranCountObj = await _unitOfWork.TransactionCounterRepository.GetEntityByIdAsync(transactionCounterId);
            if(tranCountObj is not null)
            {
                _unitOfWork.TransactionCounterRepository.SoftRemoveEntity(tranCountObj);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if(isSuccess)
                {
                    var result = _mapper.Map<TransactionCounterViewModel>(tranCountObj);
                    return new ResponseViewSuccess<TransactionCounterViewModel>(result);
                }
            }
            return new ResponseViewError<TransactionCounterViewModel>("Invalid Id, Please Check Id Again !");
        }

        public async Task<ResponseView<Pagination<TransactionCounterViewModel>>> GetCurrentTransactionCounters(int pageIndex = 0, int pageSize = 10)
        {
            var tranCount = await _unitOfWork.TransactionCounterRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionCounterViewModel>>(tranCount);
            if (tranCount is null) return new ResponseViewError<Pagination<TransactionCounterViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionCounterViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<TransactionCounterViewModel>>> GetHistoryTransactionCounters(int pageIndex = 0, int pageSize = 10)
        {
            var tranCount = await _unitOfWork.TransactionCounterRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionCounterViewModel>>(tranCount);
            if (tranCount is null) return new ResponseViewError<Pagination<TransactionCounterViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionCounterViewModel>>(result);
        }

        public async Task<ResponseView<TransactionCounterViewModel>> GetTransactionCounterById(Guid transactionCounterId)
        {
            var tranCount = await _unitOfWork.TransactionCounterRepository.FirstOrDefaultAsync(x=>x.Id==transactionCounterId);
            var result = _mapper.Map<TransactionCounterViewModel>(tranCount);
            if (tranCount is null) return new ResponseViewError<TransactionCounterViewModel>("Not Found");
            else return new ResponseViewSuccess<TransactionCounterViewModel>(result);
        }

        public async Task<ResponseView<Pagination<TransactionCounterViewModel>>> GetTransactionCountersByName(string transactionCounterName, int pageIndex = 0, int pageSize = 10)
        {
            var tranCount = await _unitOfWork.TransactionCounterRepository.ToPagination(expression: x => x.TransactionCounterName==transactionCounterName, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionCounterViewModel>>(tranCount);
            if (tranCount.Items.Count() < 1) return new ResponseViewError<Pagination<TransactionCounterViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionCounterViewModel>>(result);
        }

        public async Task<ResponseView<TransactionCounterViewModel>> UpdateTransactionCounter(Guid transactionCounterId, CreateTransactionCounterViewModel transactionCounter)
        {
            var tranCount = await _unitOfWork.TransactionCounterRepository.GetEntityByIdAsync(transactionCounterId);
            if(tranCount is not null)
            {
                _mapper.Map(transactionCounter, tranCount);
                _unitOfWork.TransactionCounterRepository.UpdateEntity(tranCount);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<TransactionCounterViewModel>(tranCount);
                    return new ResponseViewSuccess<TransactionCounterViewModel>(result);
                }
            }
            return new ResponseViewError<TransactionCounterViewModel>("Invalid Id, Please Check Id Again !");
        }
    }
}
