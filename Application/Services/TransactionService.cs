﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Application.ViewModels.TransactionViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Xml.Linq;

namespace Application.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TransactionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<ApproveTransactionViewModel>> ApproveTransaction(Guid transactionId)
        {
            var transaction = await _unitOfWork.TransactionRepository.GetEntityByIdAsync(transactionId);
            if (transaction is null)
            {
                return new ResponseViewError<ApproveTransactionViewModel>("Not found Transaction!!!");
            }
            transaction.TransactionStatus = TransactionStatusEnum.Success;
            var newTransaction = _mapper.Map<Transaction>(transaction);
            _unitOfWork.TransactionRepository.UpdateEntity(newTransaction);
            var result = _mapper.Map<ApproveTransactionViewModel>(newTransaction);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<ApproveTransactionViewModel>(result) : new ResponseViewError<ApproveTransactionViewModel>("Approve Transaction Fail!!!");
        }
        public async Task<ResponseView<ApproveTransactionViewModel>> RejectTransaction(Guid transactionId)
        {
            var transaction = await _unitOfWork.TransactionRepository.GetEntityByIdAsync(transactionId);
            if (transaction is null)
            {
                return new ResponseViewError<ApproveTransactionViewModel>("Not found Transaction!!!");
            }
            transaction.TransactionStatus = TransactionStatusEnum.Fail;
            var newTransaction = _mapper.Map<Transaction>(transaction);
            _unitOfWork.TransactionRepository.UpdateEntity(newTransaction);
            var result = _mapper.Map<ApproveTransactionViewModel>(newTransaction);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<ApproveTransactionViewModel>(result) : new ResponseViewError<ApproveTransactionViewModel>("Approve Transaction Fail!!!");
        }
        public async Task<ResponseView<TransactionViewModel>> CreateTransaction(CreateTransactionViewModel model)
        {
            var transactionCounter = await _unitOfWork.TransactionCounterRepository.GetEntityByIdAsync(model.TransactionCounterId);
            if (transactionCounter is null) return new ResponseViewError<TransactionViewModel>("Invalid TransactionCounter Id, Please Check Id Again !");
            var wallet = await _unitOfWork.WalletRepository.GetEntityByIdAsync(model.WalletId);
            if (wallet is null) return new ResponseViewError<TransactionViewModel>("Invalid Wallet Id, Please Check Id Again !");
            var order = await _unitOfWork.OrderRepository.GetEntityByIdAsync(model.OrderId);
            if (order is null) return new ResponseViewError<TransactionViewModel>("Invalid Order Id, Please Check Id Again !");

            var transaction = _mapper.Map<Transaction>(model);
            transaction.TransactionStatus = Domain.Enums.TransactionStatusEnum.Pending;
            await _unitOfWork.TransactionRepository.AddEntityAsync(transaction);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            if (isSuccess)
            {
                var result = _mapper.Map<TransactionViewModel>(transaction);
                return new ResponseViewSuccess<TransactionViewModel>(result);
            }
            return new ResponseViewError<TransactionViewModel>("Create Fail, Invalid Input Information");
        }

        public async Task<ResponseView<TransactionViewModel>> DeleteTransaction(Guid transactionId)
        {
            var transaction = await _unitOfWork.TransactionRepository.GetEntityByIdAsync(transactionId);
            if (transaction is not null)
            {
                _unitOfWork.TransactionRepository.SoftRemoveEntity(transaction);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<TransactionViewModel>(transaction);
                    return new ResponseViewSuccess<TransactionViewModel>(result);
                }
            }
            return new ResponseViewError<TransactionViewModel>("Invalid Id, Please Check Id Again !");
        }

        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetCurrentTransaction(int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }
        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetHistoryTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }
        public async Task<ResponseView<Pagination<ShopViewTransactionViewModel>>> GetTransactionsByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var shop = await _unitOfWork.ShopRepository.ToPagination
                (expression: x => x.Id == shopId,
                include: x => x
                .Include(x => x.ShopCategories)
                .ThenInclude(x => x.Products)
                .ThenInclude(x => x.OrderDetails)
                .ThenInclude(x => x.Orders)
                .ThenInclude(x => x.Transactions),
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ShopViewTransactionViewModel>>(shop);
            if (shop is null) return new ResponseViewError<Pagination<ShopViewTransactionViewModel>>("Can't get Order by ShopId");
            else return new ResponseViewSuccess<Pagination<ShopViewTransactionViewModel>>(result);
        }
        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetFailureTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(expression: x => x.TransactionStatus == TransactionStatusEnum.Fail, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetPendingTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(expression: x => x.TransactionStatus == TransactionStatusEnum.Pending, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }


        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetSuccessTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(expression: x => x.TransactionStatus == TransactionStatusEnum.Success, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction.Items is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }

        public async Task<ResponseView<TransactionViewModel>> GetTransactionById(Guid transactionId)
        {
            var transaction = await _unitOfWork.TransactionRepository.FirstOrDefaultAsync(x => x.Id == transactionId);
            var result = _mapper.Map<TransactionViewModel>(transaction);
            if (transaction is null) return new ResponseViewError<TransactionViewModel>("Not Found");
            else return new ResponseViewSuccess<TransactionViewModel>(result);
        }

        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetTransactionsByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(expression: x => x.OrderId == orderId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetTransactionsByTransactionCounterId(Guid transactionCounterId, int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(expression: x => x.TransactionCounterId == transactionCounterId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<TransactionViewModel>>> GetTransactionsByWalletId(Guid walletId, int pageIndex = 0, int pageSize = 10)
        {
            var transaction = await _unitOfWork.TransactionRepository.ToPagination(expression: x => x.WalletId == walletId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transaction);
            if (transaction is null) return new ResponseViewError<Pagination<TransactionViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<TransactionViewModel>>(result);
        }

        public async Task<ResponseView<TransactionViewModel>> UpdateTransaction(Guid transactionId, CreateTransactionViewModel model)
        {
            var transasction = await _unitOfWork.TransactionRepository.GetEntityByIdAsync(transactionId);
            if (transasction is not null)
            {
                _mapper.Map(model, transasction);
                _unitOfWork.TransactionRepository.UpdateEntity(transasction);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<TransactionViewModel>(transasction);
                    return new ResponseViewSuccess<TransactionViewModel>(result);
                }
            }
            return new ResponseViewError<TransactionViewModel>("Invalid Id, Please Check Id Again !");
        }
    }
}
