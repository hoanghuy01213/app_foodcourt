﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.Response;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IJwtService _jwtService;
        private readonly IClaimService _claimService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, IJwtService jwtService, IClaimService claimService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _jwtService = jwtService;
            _claimService = claimService;
        }

        public async Task<ResponseView<UserLoginResponse>> LoginAsync(UserLoginRequest request)
        {
            //Check UserName or Email and Password
            var user = await _unitOfWork.UserRepository.GetEntityByGenericAsync(x => x.UserName == request.UserName || x.Email == request.Email);
            if (user is null)
            {
                return new ResponseViewError<UserLoginResponse>("Not found User!!!");
            }
            if (!BCrypt.Net.BCrypt.Verify(request.Password, user.HashedPassword))
            {
                return new ResponseViewError<UserLoginResponse>("Incorrect Password!!!");
            }
            var newUser = _mapper.Map<UserLoginResponse>(user);
            newUser.Token = _jwtService.GenerateJsonWebToken(user);
            newUser.ExpireDay = DateTime.Now.AddDays(1);
            return new ResponseViewSuccess<UserLoginResponse>(newUser);
        }

        public async Task<ResponseView<bool>> RegisterAsync(UserRegisterDTO request)
        {
            //Check UserName or Email is exist or not
            var isExist = await _unitOfWork.UserRepository.GetEntityByGenericAsync(x => x.UserName == request.Username || x.Email == request.Email);
            if (isExist is not null)
            {
                return new ResponseViewError<bool>("User is already exist!!!");
            }
            var newUser = _mapper.Map<User>(request);
            await _unitOfWork.UserRepository.AddEntityAsync(newUser);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            if (isSuccess)
            {
                _mapper.Map<UserRegisterDTO>(newUser);
            }
            return new ResponseViewSuccess<bool>();
        }
        public async Task<ResponseView<Pagination<UserViewModel>>> GetHistoryUser(int pageIndex = 0, int pageSize = 10)
        {
            var result = await _unitOfWork.UserRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var users = _mapper.Map<Pagination<UserViewModel>>(result);
            return users is not null ? new ResponseViewSuccess<Pagination<UserViewModel>>(users) : new ResponseViewError<Pagination<UserViewModel>>("List user is empty!!!");
        }

        public async Task<ResponseView<Pagination<UserViewModel>>> GetCurrentUserAsync(int pageIndex = 0, int pageSize = 10)
        {
            var result = await _unitOfWork.UserRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var users = _mapper.Map<Pagination<UserViewModel>>(result);
            return users is not null ? new ResponseViewSuccess<Pagination<UserViewModel>>(users) : new ResponseViewError<Pagination<UserViewModel>>("List user is empty!!!");
        }

        public async Task<ResponseView<UserViewModel>> GetUserByIdAsync(Guid userId)
        {
            var result = await _unitOfWork.UserRepository.GetEntityByGenericAsync(x => x.Id == userId);
            var user = _mapper.Map<UserViewModel>(result);
            return user is not null ? new ResponseViewSuccess<UserViewModel>(user) : new ResponseViewError<UserViewModel>("Not found User!!!");
        }
        public async Task<ResponseView<UserUpdateRequest>> UpdateUser(Guid userId, UserUpdateRequest request)
        {
            var user = await _unitOfWork.UserRepository.GetEntityByGenericAsync(x => x.Id == userId);
            if (user is null)
            {
                return new ResponseViewError<UserUpdateRequest>("Not found User!!!");
            }
            var newUser = _mapper.Map(request, user);
            //var newUser = _mapper.Map<User>(request);
            _unitOfWork.UserRepository.UpdateEntity(user);
            var result = _mapper.Map<UserUpdateRequest>(newUser);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<UserUpdateRequest>(result) : new ResponseViewError<UserUpdateRequest>("Update User Fail!!!");
        }
        public async Task<ResponseView<ChangePasswordViewModel>> ChangePassword(Guid id, ChangePasswordViewModel changePassword)
        {
            if (!_claimService.GetCurrentUserId.Equals(id))
                return new ResponseViewError<ChangePasswordViewModel>("you are not login with this account, please login first !!!");
            var user = (await _unitOfWork.UserRepository.GetEntityByGenericAsync(x => x.Id == id));
            if (user is null) return new ResponseViewError<ChangePasswordViewModel>("forbidden exception!");
            if (!BCrypt.Net.BCrypt.Verify(changePassword.OldPassword, user.HashedPassword))
                return new ResponseViewError<ChangePasswordViewModel>("Wrong password !!!");
            if (string.CompareOrdinal(changePassword.NewPassword, changePassword.ConfirmPassword) is not 0)
            {
                return new ResponseViewError<ChangePasswordViewModel>("the new password and confirm password does not match!!!");
            }
            user.HashedPassword = StringUtils.Hash(changePassword.NewPassword);
            _unitOfWork.UserRepository.UpdateEntity(user);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess
                ? new ResponseViewSuccess<ChangePasswordViewModel>(changePassword)
                : new ResponseViewError<ChangePasswordViewModel>("Change Password Fail!!!");
        }
    }
}