﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.Response;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.TransactionViewModels;
using Application.ViewModels.WalletViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;
using System.Xml.Linq;

namespace Application.Services
{
    public class WalletService : IWalletService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public WalletService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseView<WalletPermission>> ApproveWallet(Guid walletId)
        {
            var wallet = await _unitOfWork.WalletRepository.GetEntityByGenericAsync(x => x.Id == walletId);
            if (wallet is null)
            {
                return new ResponseViewError<WalletPermission>("Not found Transaction!!!");
            }
            wallet.WalletStatus = WalletStatusEnum.Active;
            var newWallet = _mapper.Map<Wallet>(wallet);
            _unitOfWork.WalletRepository.UpdateEntity(newWallet);
            var result = _mapper.Map<WalletPermission>(newWallet);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<WalletPermission>(result) : new ResponseViewError<WalletPermission>("Approve Wallet Fail!!!");
        }

        public async Task<ResponseView<WalletPermission>> RejectWallet(Guid walletId)
        {
            var wallet = await _unitOfWork.WalletRepository.GetEntityByGenericAsync(x => x.Id == walletId);
            if (wallet is null)
            {
                return new ResponseViewError<WalletPermission>("Not found Transaction!!!");
            }
            wallet.WalletStatus = WalletStatusEnum.Inactive;
            var newWallet = _mapper.Map<Wallet>(wallet);
            _unitOfWork.WalletRepository.UpdateEntity(newWallet);
            var result = _mapper.Map<WalletPermission>(newWallet);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new ResponseViewSuccess<WalletPermission>(result) : new ResponseViewError<WalletPermission>("Approve Wallet Fail!!!");
        }

        public async Task<ResponseView<WalletViewModel>> CreateWallet(CreateWalletViewModel model)
        {
            var checkId = await _unitOfWork.CardRepository.GetEntityByIdAsync(model.CardId);
            if (checkId != null)
            {
                var wallet = _mapper.Map<Wallet>(model);
                wallet.WalletStatus = Domain.Enums.WalletStatusEnum.Inactive;
                await _unitOfWork.WalletRepository.AddEntityAsync(wallet);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<WalletViewModel>(wallet);
                    return new ResponseViewSuccess<WalletViewModel>(result);
                }
                return new ResponseViewError<WalletViewModel>("Create Fail, Invalid Input Information");
            }
            return new ResponseViewError<WalletViewModel>("Invalid Card Id, Please Check Id Again !");
        }

        public async Task<ResponseView<WalletViewModel>> DeleteWallet(Guid walletId)
        {
            var wallet = await _unitOfWork.WalletRepository.GetEntityByIdAsync(walletId);
            if (wallet is not null)
            {
                _unitOfWork.WalletRepository.SoftRemoveEntity(wallet);
                var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    var result = _mapper.Map<WalletViewModel>(wallet);
                    return new ResponseViewSuccess<WalletViewModel>(result);
                }
            }
            return new ResponseViewError<WalletViewModel>("Invalid Id, Please Check Id Again");
        }

        public async Task<ResponseView<Pagination<WalletViewModel>>> GetActiveWallets(int pageIndex = 0, int pageSize = 10)
        {
            var wallet = await _unitOfWork.WalletRepository.ToPagination(x => x.WalletStatus == WalletStatusEnum.Active, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<WalletViewModel>>(wallet);
            if (wallet is null) return new ResponseViewError<Pagination<WalletViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<WalletViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<WalletViewModel>>> GetCurrentWallet(int pageIndex = 0, int pageSize = 10)
        {
            var wallet = await _unitOfWork.WalletRepository.ToPagination(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<WalletViewModel>>(wallet);
            if (wallet is null) return new ResponseViewError<Pagination<WalletViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<WalletViewModel>>(result);
        }
        public async Task<ResponseView<Pagination<WalletViewModel>>> GetHistoryWallet(int pageIndex = 0, int pageSize = 10)
        {
            var wallet = await _unitOfWork.WalletRepository.ToPaginationNoQueryFilter(pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<WalletViewModel>>(wallet);
            if (wallet is null) return new ResponseViewError<Pagination<WalletViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<WalletViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<WalletViewModel>>> GetInActiveWallets(int pageIndex = 0, int pageSize = 10)
        {
            var wallet = await _unitOfWork.WalletRepository.ToPagination(x => x.WalletStatus == WalletStatusEnum.Inactive, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<WalletViewModel>>(wallet);
            if (wallet is null) return new ResponseViewError<Pagination<WalletViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<WalletViewModel>>(result);
        }

        public async Task<ResponseView<WalletViewModel>> GetWalletById(Guid walletId)
        {
            var wallet = await _unitOfWork.WalletRepository.GetEntityByGenericAsync(x => x.Id == walletId);
            var result = _mapper.Map<WalletViewModel>(wallet);
            if (wallet is null) return new ResponseViewError<WalletViewModel>("Not Found");
            else return new ResponseViewSuccess<WalletViewModel>(result);
        }

        public async Task<ResponseView<Pagination<WalletViewModel>>> GetWalletsByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10)
        {
            var wallet = await _unitOfWork.WalletRepository.ToPagination(expression: x => x.CardId == cardId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<WalletViewModel>>(wallet);
            if (wallet is null) return new ResponseViewError<Pagination<WalletViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<WalletViewModel>>(result);
        }

        public async Task<ResponseView<Pagination<WalletViewModel>>> GetHistoryWalletsByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10)
        {
            var wallet = await _unitOfWork.WalletRepository.ToPaginationNoQueryFilter(expression: x => x.CardId == cardId, pageIndex: pageIndex, pageSize: pageSize, sortColumn: x => x.CreationDate, sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<WalletViewModel>>(wallet);
            if (wallet is null) return new ResponseViewError<Pagination<WalletViewModel>>("Not Found");
            else return new ResponseViewSuccess<Pagination<WalletViewModel>>(result);
        }

        public async Task<ResponseView<WalletViewModel>> UpdateWallet(Guid walletId, CreateWalletViewModel walletDTO)
        {
            var checkId = await _unitOfWork.CardRepository.GetEntityByIdAsync(walletDTO.CardId);
            if (checkId != null)
            {
                var wallet = await _unitOfWork.WalletRepository.GetEntityByIdAsync(walletId);
                if (wallet != null)
                {
                    _mapper.Map(walletDTO, wallet);
                    _unitOfWork.WalletRepository.UpdateEntity(wallet);
                    var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
                    if (isSuccess)
                    {
                        var result = _mapper.Map<WalletViewModel>(wallet);
                        return new ResponseViewSuccess<WalletViewModel>(result);
                    }
                }
                return new ResponseViewError<WalletViewModel>("Invalid Wallet Id, Please Check Id Again !");
            }
            return new ResponseViewError<WalletViewModel>("Invalid Card Id, Please Check Id Again !");
        }
    }
}
