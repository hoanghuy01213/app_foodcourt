﻿namespace Application.Utils
{
    public static class StringUtils
    {
        public static string Hash(this string input) => BCrypt.Net.BCrypt.HashPassword(input);
    }
}
