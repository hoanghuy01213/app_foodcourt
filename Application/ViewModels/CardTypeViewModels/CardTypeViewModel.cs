﻿namespace Application.ViewModels.CardTypeViewModels
{
    public class CardTypeViewModel
    {
        public Guid CardTypeId { get; set; }
        public string Avatar { get; set; }
        public decimal Points { get; set; }
        public decimal Discount { get; set; }
        public DateTime? CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
