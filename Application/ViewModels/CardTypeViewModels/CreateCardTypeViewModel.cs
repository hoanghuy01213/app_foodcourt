﻿namespace Application.ViewModels.CardTypeViewModels
{
    public class CreateCardTypeViewModel
    {
        public string Avatar { get; set; }
        public decimal Points { get; set; }
        public decimal Discount { get; set; }
    }
}
