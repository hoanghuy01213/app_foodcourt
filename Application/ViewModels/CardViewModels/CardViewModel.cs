﻿namespace Application.ViewModels.CardViewModels
{
    public class CardViewModel
    {
        public Guid Id { get; set; }
        public string CardNumber { get; set; }
        public string SecurityCode { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Guid CardTypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public string? CreatedByEmail { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
