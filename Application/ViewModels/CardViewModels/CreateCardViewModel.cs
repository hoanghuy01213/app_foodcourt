﻿namespace Application.ViewModels.CardViewModels
{
    public class CreateCardViewModel
    {
        public string CardNumber { get; set; }
        public string SecurityCode { get; set; }
        public Guid CardTypeId { get; set; }
    }
}
