﻿using Domain.Enums;

namespace Application.ViewModels.OrderViewModels
{
    public class ApproveOrderViewModel
    {
        public OrderStatus OrderStatus { get; set; }
    }
}
