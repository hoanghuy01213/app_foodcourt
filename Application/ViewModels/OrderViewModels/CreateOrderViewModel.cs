﻿using Application.ViewModels.OrderDetailViewModels;

namespace Application.ViewModels.OrderViewModels
{
    public class CreateOrderViewModel
    {
        public Guid CardId { get; set; }
        public ICollection<AddOrderDetailViewModel> OrderDetails { get; set; }
    }
}
