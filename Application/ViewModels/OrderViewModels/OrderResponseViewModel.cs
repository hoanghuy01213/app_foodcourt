﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.OrderViewModels
{
    public class OrderResponseViewModel
    {
        public Guid OrderId { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalAmount { get; set; }
        public Guid CardId { get; set; }
        public List<OrderDetails> OrderDetails { get; set; }
    }
    public class OrderDetails
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
