﻿using Domain.Enums;

namespace Application.ViewModels.OrderViewModels
{
    public class OrderViewModel
    {
        public Guid OrderId { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public decimal TotalAmount { get; set; }
        public Guid CardId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
