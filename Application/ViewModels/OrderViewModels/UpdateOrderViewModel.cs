﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.OrderViewModels
{
    public class UpdateOrderViewModel
    {
        //public decimal TotalAmount { get; set; }
        public Guid CardId { get; set; }
        public DateTime? OrderDate { get; set; }
    }
}
