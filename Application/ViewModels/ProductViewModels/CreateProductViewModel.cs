﻿namespace Application.ViewModels.ProductViewModels
{
    public class CreateProductViewModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string UrlImage { get; set; }
        public Guid ShopCategoryId { get; set; }
    }
    public class TopSellingProduct
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int TotalQuantity { get; set; }
    }
}
