﻿using System.Net;

namespace Application.ViewModels.Response
{
    public class Response
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }

        public Response(HttpStatusCode status, string message, object result)
        {
            Status = status.ToString();
            Message = message;
            Result = result;
        }
        public Response(HttpStatusCode status, string message)
        {
            Status = status.ToString();
            Message = message;
        }
    }
}
