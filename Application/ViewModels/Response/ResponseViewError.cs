﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.Response
{
    public class ResponseViewError<T> : ResponseView<T>
    {
        public List<string> ValidationErrors { get; set; } = new List<string>();
        public ResponseViewError(string message)
        {
            StatusCode = HttpStatusCode.BadRequest;
            Succeeded = false;
            Message = message;
            ValidationErrors.Add(message);
        }
        public ResponseViewError(string message, List<string> validationErrors)
        {
            StatusCode = HttpStatusCode.BadRequest;
            Succeeded = false;
            Message = message;
            ValidationErrors = validationErrors;
        }
    }
}
