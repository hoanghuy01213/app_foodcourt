﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.Response
{
    public class ResponseViewSuccess<T> : ResponseView<T>
    {
        public ResponseViewSuccess(T result)
        {
            StatusCode = HttpStatusCode.OK;
            Succeeded = true;
            Message = "Success";
            Result= result;
        }
        public ResponseViewSuccess()
        {
            StatusCode = HttpStatusCode.OK;
            Succeeded = true;
            Message = "Success";
        }
    }
}
