﻿namespace Application.ViewModels.ShopCategoryViewModels
{
    public class CreateShopCategoryViewModel
    {
        public Guid ShopId { get; set; }
        public string Name { get; set; }
    }
}
