﻿using Domain.Entities;

namespace Application.ViewModels.ShopCategoryViewModels
{
    public class ShopCategoryViewModel
    {
        public Guid ShopCategoryId { get; set; }
        public string Name { get; set; }
        public Guid ShopId { get; set; }
        public DateTime? CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
