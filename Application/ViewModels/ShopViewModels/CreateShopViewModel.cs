﻿namespace Application.ViewModels.ShopsViewModels
{
    public class CreateShopViewModel
    {
        public string ShopName { get; set; }
        public string Location { get; set; }
        public Guid UserId { get; set; }
    }
}
