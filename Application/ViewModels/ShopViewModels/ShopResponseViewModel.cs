﻿using Domain.Entities;

namespace Application.ViewModels.ShopsViewModels
{
    public class ShopResponseViewModel
    {
        public Guid ShopId { get; set; }
        public string ShopName { get; set; }
        public string Location { get; set; }
        public Guid UserId { get; set; }
        public List<ShopCategories> ShopCategories { get; set; }
        public DateTime? CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class ShopCategories
    {
        public Guid ShopId { get; set; }
        public string Name { get; set; }
    }
}
    