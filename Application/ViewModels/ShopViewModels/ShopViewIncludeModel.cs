﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ShopViewModels
{
    public class UserViewIncludeModel
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public ICollection<ShopViewIncludeModel> Shop { get; set; }
    }

    public class ShopViewIncludeModel
    {
        public Guid ShopId { get; set; }
        public string ShopName { get; set; }
        public string Location { get; set; }
        public Guid UserId { get; set; }
        public ICollection<ViewShopCategory> ViewShopCategory { get; set; }
    }
    public class ViewShopCategory
    {
        public Guid ShopCategoryId { get; set; }
        public ICollection<ViewProduct> ViewProduct { get; set; }
    }
    public class ViewProduct
    {
        public Guid ProductId { get; set; }
        public ICollection<ViewOrderDetail> ViewOrderDetail { get; set; }
    }

    public class ViewOrderDetail
    {
        public Guid OrderId { get; set; }
        public int Quantity { get; set; }
        public OrderEntity Orders { get; set; }
    }
    public class OrderEntity
    {
        public Guid OrderId { get; set; }
        public Guid CardId { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
