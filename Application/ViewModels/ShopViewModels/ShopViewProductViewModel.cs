﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ShopViewModels
{

    public class ShopViewProductViewModel
    {
        public Guid ShopId { get; set; }
        public string ShopName { get; set; }
        public string Location { get; set; }
        public Guid UserId { get; set; }
        public ICollection<ShopCategoryProduct> ShopCategoryProduct { get; set; }
    }
    public class ShopCategoryProduct
    {
        public Guid ShopCategoryId { get; set; }
        public ICollection<ProductShop> ProductShop { get; set; }

    }
    public class ProductShop
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string UrlImage { get; set; }
    }
}
