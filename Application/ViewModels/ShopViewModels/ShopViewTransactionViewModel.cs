﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ShopViewModels
{

    public class ShopViewTransactionViewModel
    {
        public Guid ShopId { get; set; }
        public string ShopName { get; set; }
        public string Location { get; set; }
        public Guid UserId { get; set; }
        public ICollection<ShopCategoryTransaction> ShopCategoryTransaction { get; set; }
    }
    public class ShopCategoryTransaction
    {
        public Guid ShopCategoryId { get; set; }
        public ICollection<ShopProductTransaction> ShopProductTransaction { get; set; }
    }
    public class ShopProductTransaction
    {
        public Guid ProductId { get; set; }
        public ICollection<ShopProductTransactionOrderDetail> ShopProductTransactionOrderDetails { get; set; }
    }
    public class ShopProductTransactionOrderDetail
    {
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public OrderTransaction Order { get; set; }
    }
    public class OrderTransaction
    {
        public ICollection<MyTransaction> Transaction { get; set; }
    }
    public class MyTransaction
    {
        public Guid TransactionId { get; set; }
        public decimal Money { get; set; }
        public TransactionStatusEnum TransactionStatus { get; set; }
        public Guid OrderId { get; set; }
        public Guid? WalletId { get; set; }
        public Guid TransactionCounterId { get; set; }
    }

}
