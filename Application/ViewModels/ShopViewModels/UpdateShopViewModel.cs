﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ShopsViewModels
{
    public class UpdateShopViewModel
    {
        public string ShopName { get; set; }
        public string Location { get; set; }
    }
}
