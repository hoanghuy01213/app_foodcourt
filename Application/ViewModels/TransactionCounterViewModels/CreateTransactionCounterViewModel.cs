﻿namespace Application.ViewModels.TransactionCounterViewModels
{
    public class CreateTransactionCounterViewModel
    {
        public string TransactionCounterName { get; set; }
        public string Location { get; set; }
    }
}
