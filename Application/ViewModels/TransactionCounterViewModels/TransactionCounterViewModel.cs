﻿namespace Application.ViewModels.TransactionCounterViewModels
{
    public class TransactionCounterViewModel
    {
        public Guid TransactionCounterId { get; set; }
        public string TransactionCounterName { get; set; }
        public string Location { get; set; }
        public DateTime? CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
