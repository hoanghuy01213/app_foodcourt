﻿using Domain.Enums;

namespace Application.ViewModels.TransactionViewModels
{
    public class ApproveTransactionViewModel
    {
        public TransactionStatusEnum TransactionStatus { get; set; }
    }
}
