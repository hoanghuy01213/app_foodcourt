﻿using Domain.Entities;
using Domain.Enums;

namespace Application.ViewModels.TransactionViewModels
{
    public class CreateTransactionViewModel
    {
        public decimal Money { get; set; }
        public Guid OrderId { get; set; }
        public Guid WalletId { get; set; }
        public Guid TransactionCounterId { get; set; }
    }
}
