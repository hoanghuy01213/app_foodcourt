﻿using Domain.Enums;

namespace Application.ViewModels.TransactionViewModels
{
    public class TransactionViewModel
    {
        public Guid TransactionId { get; set; }
        public decimal Money { get; set; }
        public TransactionStatusEnum TransactionStatus { get; set; }
        public Guid OrderId { get; set; }
        public Guid? WalletId { get; set; }
        public Guid TransactionCounterId { get; set; }
        public DateTime? CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
