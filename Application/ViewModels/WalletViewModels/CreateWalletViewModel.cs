﻿using Domain.Enums;

namespace Application.ViewModels.WalletViewModels
{
    public class CreateWalletViewModel
    {
        public decimal Remainder { get; set; }
        public Guid CardId { get; set; }
    }
}
