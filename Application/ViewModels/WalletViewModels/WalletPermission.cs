﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.WalletViewModels
{
    public class WalletPermission
    {
        public WalletStatusEnum WalletStatus { get; set; }
    }
}
