﻿using Domain.Enums;

namespace Application.ViewModels.WalletViewModels
{
    public class WalletViewModel
    {
        public Guid Id { get; set; }
        public WalletStatusEnum WalletStatus { get; set; }
        public decimal Remainder { get; set; }
        public Guid CardId { get; set; }
        public DateTime? CreationDate { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public Guid? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public Guid? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
