﻿namespace Domain.Entities
{
    public class Card:BaseEntity
    {
        public string CardNumber { get; set; }
        public string SecurityCode { get; set; }
        public DateTime ExpirationDate { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<Wallet> Wallets { get; set; }
        public Guid CardTypeId { get; set; }
        public CardType CardType { get; set; }
    }
}
