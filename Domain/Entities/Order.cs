﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Order:BaseEntity
    {
        public Guid CardId{ get; set; }
        public OrderStatus OrderStatus { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime OrderDate { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
        public Card Card { get; set; }
    }
}
