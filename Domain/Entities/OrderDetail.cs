﻿namespace Domain.Entities
{
    public class OrderDetail:BaseEntity
    {
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public Order Orders { get; set; }
        public Product Products { get; set; }
    }
}
