﻿namespace Domain.Entities
{
    public class Shop:BaseEntity
    {
        public string ShopName { get; set; }
        public string Location { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public ICollection<ShopCategory> ShopCategories { get; set; }
    }
}
