﻿namespace Domain.Entities
{
    public class ShopCategory:BaseEntity
    {
        public Guid ShopId { get; set; }
        public Shop Shops { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
