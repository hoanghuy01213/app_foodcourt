﻿namespace Domain.Entities
{
    public class TransactionCounter : BaseEntity
    {
        public string TransactionCounterName { get; set; }
        public string Location { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}
