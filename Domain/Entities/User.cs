﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class User:BaseEntity
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string HashedPassword { get; set; }
        public decimal CreditBalance { get; set; }
        public decimal Point { get; set; }
        public ICollection<Shop> Shops { get; set; }
        public UserRoleEnum Role { get; set; } = UserRoleEnum.User;
    }
}
