﻿namespace Domain.Enums
{
    public enum WalletStatusEnum
    {
        Active = 0,
        Inactive = 1
    }
}
