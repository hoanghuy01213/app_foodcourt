﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.CardViewModels;
using Application.ViewModels.Response;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class CardController : BaseController
    {
        private readonly ICardService _cardService;
        public CardController(ICardService cardService)
        {
            _cardService = cardService;
        }
        [HttpPost("CreateCard")]
        public async Task<ActionResult<ResponseView<CardViewModel>>> CreateCard(CreateCardViewModel model)
        {
            var response = await _cardService.CreateCard(model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpGet("GetCurrentCard")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<CardViewModel>>>> GetCurrentCard(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _cardService.GetCurrentCard(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpGet("GetHistoryCard")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<CardViewModel>>>> GetHistoryCard(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _cardService.GetHistoryCard(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpGet("GetCardById/{cardId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<CardViewModel>>> GetCardById(Guid cardId)
        {
            var response = await _cardService.GetCardById(cardId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCardByCardTypeId/{cardTypeId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<CardViewModel>>>> GetCardsByCardTypeId(Guid cardTypeId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _cardService.GetCardsByCardTypeId(cardTypeId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateCard/{cardId}")]
        public async Task<ActionResult<ResponseView<CardViewModel>>> UpdateCard(Guid cardId, UpdateCardViewModel model)
        {
            var response = await _cardService.UpdateCard(cardId, model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteCard/{cardId}")]
        public async Task<ActionResult<ResponseView<CardViewModel>>> DeleteCard(Guid cardId)
        {
            var response = await _cardService.DeleteCard(cardId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
