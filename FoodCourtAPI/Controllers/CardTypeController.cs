﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.CardTypeViewModels;
using Application.ViewModels.Response;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class CardTypeController : BaseController
    {
        private readonly ICardTypeService _cardTypeService;

        public CardTypeController(ICardTypeService cardTypeService)
        {
            _cardTypeService = cardTypeService;
        }

        [HttpPost("CreateCardType")]
        public async Task<ActionResult<ResponseView<CardTypeViewModel>>> CreateCardType(CreateCardTypeViewModel createCardTypeViewModel)
        {
            var response = await _cardTypeService.CreateCardType(createCardTypeViewModel);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetAllCardTypes")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<CardTypeViewModel>>>> GetCurrentCardType(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _cardTypeService.GetCurrentCardType(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryCardType")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<CardTypeViewModel>>>> GetHistoryCardType(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _cardTypeService.GetCurrentCardType(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCardTypeById/{cardTypeId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<CardTypeViewModel>>> GetCardTypeById(Guid cardTypeId)
        {
            var response = await _cardTypeService.GetCardTypeById(cardTypeId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateCardType/{cardTypeId}")]
        public async Task<ActionResult<ResponseView<CardTypeViewModel>>> UpdateCardType(Guid cardTypeId, CreateCardTypeViewModel model)
        {
            var response = await _cardTypeService.UpdateCardType(cardTypeId, model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteCardType/{cardTypeId}")]
        public async Task<ActionResult<ResponseView<CardTypeViewModel>>> DeleteCardType(Guid cardTypeId)
        {
            var response = await _cardTypeService.DeleteCardType(cardTypeId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
