﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Azure;
using Azure.Core;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("CreateOrder")]
        [Authorize(Policy = "User")]
        public async Task<ActionResult<ResponseView<OrderResponseViewModel>>> CreateOrder(CreateOrderViewModel model)
        {
            var response = await _orderService.CreateOrder(model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteOrder/{orderId}")]
        public async Task<ActionResult> DeleteOrder(Guid orderId)
        {
            var response = await _orderService.DeleteOrder(orderId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateOrder/{orderId}")]
        public async Task<ActionResult> UpdateOrder(Guid orderId, UpdateOrderViewModel request)
        {
            var response = await _orderService.Update(orderId, request);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentOrder")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetCurrentOrder(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetCurrentOrder(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryOrder")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetHistoryOrder(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetHistoryOrder(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetOrderByShopId/{shopId}")]
        public async Task<ActionResult<ResponseView<Pagination<ShopViewIncludeModel>>>> GetOrderByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetOrdersByShopId(shopId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetOrdersByUserId/{userId}")]
        public async Task<ActionResult<ResponseView<Pagination<UserViewIncludeModel>>>> GetOrdersByUserId(Guid userId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetOrdersByUserId(userId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpGet("GetPendingOrders")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetPendingOrders(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetPendingOrders(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetSuccessOrders")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetSuccessOrders(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetSuccessOrders(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetFailOrders")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetFailOrders(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetFailOrders(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetOrdersByCardId/{cardId}")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetOrdersByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetOrdersByCardId(cardId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetOrderByOrderId/{orderId}")]
        public async Task<ActionResult<ResponseView<Pagination<OrderViewModel>>>> GetOrderById(Guid orderId)
        {
            var response = await _orderService.GetOrderById(orderId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpPatch("RejectOrder/{orderId}")]
        [Authorize(Policy = "Admin")]
        public async Task<ActionResult> RejectOrder(Guid orderId)
        {
            var response = await _orderService.RejectOrder(orderId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpPatch("ApproveOrder/{orderId}")]
        [Authorize(Policy = "Admin")]
        public async Task<ActionResult> ApproveOrder(Guid orderId)
        {
            var response = await _orderService.ApproveOrder(orderId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
