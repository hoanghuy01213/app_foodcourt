﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Application.Services;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class OrderDetailController : BaseController
    {
        private readonly IOrderDetailService _orderDetailService;

        public OrderDetailController(IOrderDetailService orderDetailService)
        {
            _orderDetailService = orderDetailService;
        }

        [HttpGet("{orderId}/orderItems")]
        public async Task<ActionResult<ResponseView<Pagination<OrderResponseViewModel>>>> GetOrder(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderDetailService.GetOrderDetailByOrderId(orderId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryOrderDetailByOrderId/{orderId}/orderItems")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<OrderResponseViewModel>>>> GetHistoryOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderDetailService.GetHistoryOrderDetailByOrderId(orderId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentOrderDetailByOrderId/{orderId}/orderItems")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<OrderResponseViewModel>>>> GetCurrentOrderDetailByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderDetailService.GetCurrentOrderDetailByOrderId(orderId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPost("{orderId}/orderItems")]
        [Authorize(Policy = "user")]
        public async Task<ActionResult<ResponseView<OrderResponseViewModel>>> CreateOrderDetail(Guid orderId, CreateOrderDetailViewModel request)
        {
            var response = await _orderDetailService.AddOrder(orderId, request);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("{orderId}/orderItems")]
        public async Task<ActionResult<ResponseView<OrderResponseViewModel>>> UpdateOrderDetail(Guid orderId, UpdateOrderDetailViewModel request)
        {
            var response = await _orderDetailService.UpdateOrderDetail(orderId, request);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("{orderId}/orderItems/{productId}")]
        public async Task<ActionResult<ResponseView<OrderResponseViewModel>>> DeleteOrderDetail(Guid orderId, Guid productId)
        {
            var response = await _orderDetailService.DeleteOrderDetail(orderId, productId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
