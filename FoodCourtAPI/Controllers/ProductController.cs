﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Azure;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost("CreateProduct")]
        public async Task<ActionResult<ResponseView<ProductViewModel>>> CreateProduct(CreateProductViewModel productModel)
        {
            var response = await _productService.CreateProduct(productModel);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentProduct")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ProductViewModel>>>> GetCurrentProduct(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetCurrentProduct(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryProduct")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ProductViewModel>>>> GetHistoryProduct(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetHistoryProduct(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryProductsByShopCategoryId/{shopCategoryId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ProductViewModel>>>> GetHistoryProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetHistoryProductsByShopCategoryId(shopCategoryId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetProductById/{productId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<ProductViewModel>>> GetProductById(Guid productId)
        {
            var response = await _productService.GetProductById(productId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpGet("GetProductsByShopCategoryId/{shopCategoryId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ProductViewModel>>>> GetProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetProductsByShopCategoryId(shopCategoryId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetProductsByName/{productName}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ProductViewModel>>>> GetProductsByName(string productName, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetProductsByName(productName, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetProductByShopId/{shopId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopViewIncludeModel>>>> GetProductByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetProductByShopId(shopId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }


        [HttpPut("UpdateProduct/{productId}")]
        public async Task<ActionResult<ResponseView<ProductViewModel>>> UpdateProduct(Guid productId, CreateProductViewModel model)
        {
            var response = await _productService.UpdateProduct(productId, model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);

        }

        [HttpDelete("DeleteProduct/{productId}")]
        public async Task<ActionResult<ResponseView<ProductViewModel>>> DeleteProduct(Guid productId)
        {
            var response = await _productService.RemoveProduct(productId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTopSellers/start={start}&end={end}")]
        public async Task<ActionResult<Pagination<TopSellingProduct>>> GetTopSellingProducts(
           DateTime start,
           DateTime end,
           int pageIndex = 0,
           int pageSize = 10)
        {
            var response = await _productService.GetTopSellingProducts(start, end, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
