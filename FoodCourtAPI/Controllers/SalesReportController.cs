﻿using Application.Interfaces;
using Application.IRepositories;
using Application.ViewModels.Response;
using Application.ViewModels.SaleReportViewmodels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    [Authorize(Policy = "Admin")]
    public class SalesReportController : BaseController
    {
        private readonly ISaleReportService _saleReportService;

        public SalesReportController(ISaleReportService saleReportService)
        {
            _saleReportService = saleReportService;
        }

        [HttpGet("GetSalesByMonth/{year}/{month}")]
        public async Task<ActionResult<ResponseView<SaleReportViewModel>>> GetSalesByMonth(int year, int month)
        {
            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);
            var response = await _saleReportService.GetSalesReport(startDate, endDate);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetSalesByDateRange")]
        public async Task<ActionResult<ResponseView<SaleReportViewModel>>> GetSalesByDateRange(DateTime startDate, DateTime endDate)
        {
            var response = await _saleReportService.GetSalesReport(startDate, endDate);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetSalesReportByEachMonth")]
        public async Task<ActionResult<ResponseView<List<SaleReportViewModel>>>> GetSalesReportByEachMonth()
        {
            var response = await _saleReportService.GetSalesReportByEachMonth();
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
