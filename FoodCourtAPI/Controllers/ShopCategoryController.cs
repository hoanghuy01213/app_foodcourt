﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.Response;
using Application.ViewModels.ShopCategoryViewModels;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class ShopCategoryController : BaseController
    {
        private readonly IShopCategoryService _shopCategoryService;

        public ShopCategoryController(IShopCategoryService shopCategoryService)
        {
            _shopCategoryService = shopCategoryService;
        }

        [HttpPost("CreateShopCategory")]
        public async Task<ActionResult<ResponseView<ShopCategoryViewModel>>> CreateShopCategory(CreateShopCategoryViewModel shopCateModel)
        {
            var response = await _shopCategoryService.CreateShopCategory(shopCateModel);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentShopCategory")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopCategoryViewModel>>>> GetCurrentShopCategory(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopCategoryService.GetCurrentShopCategory(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryShopCategory")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopCategoryViewModel>>>> GetHistoryShopCategory(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopCategoryService.GetCurrentShopCategory(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetShopCategoryById/{shopCategoryId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<ShopCategoryViewModel>>> GetShopCategoryById(Guid shopCategoryId)
        {
            var response = await _shopCategoryService.GetShopCategoryById(shopCategoryId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryShopCategoriesByShopId/{shopId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<ShopCategoryViewModel>>> GetHistoryShopCategoriesByShopId(Guid shopId)
        {
            var response = await _shopCategoryService.GetHistoryShopCategoriesByShopId(shopId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetShopCategoriesByShopId/{shopId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopCategoryViewModel>>>> GetShopCategoriesByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopCategoryService.GetShopCategoriesByShopId(shopId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }


        [HttpGet("GetShopCategoriesByName/{categoryName}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopCategoryViewModel>>>> GetShopCategoriesByName(string categoryName, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopCategoryService.GetShopCategoriesByName(categoryName, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateShopCategory/{shopCategoryId}")]
        public async Task<ActionResult<ResponseView<ShopCategoryViewModel>>> UpdateShopCategory(Guid shopCategoryId, CreateShopCategoryViewModel model)
        {
            var response = await _shopCategoryService.UpdateShopCategory(shopCategoryId, model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteShopCategory/{shopCategoryId}")]
        public async Task<ActionResult<ResponseView<ShopCategoryViewModel>>> DeleteShopCategory(Guid shopCategoryId)
        {
            var response = await _shopCategoryService.DeleteShopCategory(shopCategoryId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
