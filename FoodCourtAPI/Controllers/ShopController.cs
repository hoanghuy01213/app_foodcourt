﻿using Application.Interfaces;
using Application.ViewModels.ShopsViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Results;
using System.Net;
using Application.Services;
using Microsoft.AspNetCore.Authorization;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.Response;
using Application.Commons;
using Application.ViewModels.ProductViewModels;
using Domain.Entities;
using System.Drawing.Printing;

namespace FoodCourtAPI.Controllers
{
    public class ShopController : BaseController
    {
        private readonly IShopService _shopService;

        public ShopController(IShopService shopService)
        {
            _shopService = shopService;
        }

        [HttpPost("CreateShop")]
        public async Task<ActionResult<ResponseView<ShopViewModel>>> CreateShop(CreateShopViewModel shopModel)
        {
            var response = await _shopService.CreateShop(shopModel);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentShops")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopViewModel>>>> GetCurrentShops(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopService.GetCurrentShop(pageIndex,pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryShop")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopViewModel>>>> GetHistoryShop(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopService.GetHistoryShop(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetShopByShopId/{shopId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<ShopResponseViewModel>>> GetShopByShopId(Guid shopId)
        {
            var response = await _shopService.GetShopById(shopId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetShopByUserId/{userId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<ShopResponseViewModel>>> GetShopByUserId(Guid userId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopService.GetShopByUserId(userId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetShopDetails/{shopId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopResponseViewModel>>>> GetShopDetails(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopService.GetShopDetails(shopId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetShopsByName/{shopName}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopResponseViewModel>>>> GetShopsByName(string shopName, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _shopService.GetShopByName(shopName, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateShop/{shopId}")]
        public async Task<ActionResult<ResponseView<ShopResponseViewModel>>> UpdateShop(Guid shopId, UpdateShopViewModel shopDTO)
        {
            var response = await _shopService.UpdateShop(shopId, shopDTO);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteShop/{shopId}")]
        public async Task<ActionResult<ResponseView<ShopResponseViewModel>>> DeleteShop(Guid shopId)
        {
            var response = await _shopService.DeleteShop(shopId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
