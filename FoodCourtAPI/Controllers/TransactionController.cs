﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.Response;
using Application.ViewModels.ShopViewModels;
using Application.ViewModels.TransactionViewModels;
using Azure;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class TransactionController : BaseController
    {
        private readonly ITransactionService _transactionService;
        private readonly IValidator<CreateTransactionViewModel> _createValidator;
        public TransactionController(ITransactionService transactionService, IValidator<CreateTransactionViewModel> createValidator)
        {
            _transactionService = transactionService;
            _createValidator = createValidator;
        }

        [HttpPost("CreateTransaction")]
        public async Task<ActionResult<ResponseView<TransactionViewModel>>> CreateTransaction(CreateTransactionViewModel model)
        {
            var response = await _transactionService.CreateTransaction(model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryTransaction")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetHistoryTransaction(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetHistoryTransactions(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentTransaction")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetCurrentTransaction(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetCurrentTransaction(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetSuccessTransactions")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetSuccessTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetSuccessTransactions(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetFailTransactions")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetFailTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetFailureTransactions(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }


        [HttpGet("GetPendingTransactions")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetPendingTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetPendingTransactions(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTransactionsByOrderId/{orderId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetTransactionsByOrderId(Guid orderId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetTransactionsByOrderId(orderId, pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTransactionByWalletId/{walletId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetTransactionByWalletId(Guid walletId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetTransactionsByWalletId(walletId, pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
        [HttpGet("GetTransactionsByTransactionCounterId/{transactionCounterId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetTransactionsByTransactionCounterId(Guid transactionCounterId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetTransactionsByTransactionCounterId(transactionCounterId, pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryTransactions")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionViewModel>>>> GetHistoryTransactions(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetHistoryTransactions(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTransactionsByShopId/{shopId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<ShopViewTransactionViewModel>>>> GetTransactionsByShopId(Guid shopId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionService.GetTransactionsByShopId(shopId, pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTransactionById/{transactionId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<TransactionViewModel>>> GetTransationById(Guid transactionId)
        {
            var response = await _transactionService.GetTransactionById(transactionId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateTransaction/{transactionId}")]
        public async Task<ActionResult<ResponseView<TransactionViewModel>>> UpdateTransaction(Guid transactionId, CreateTransactionViewModel model)
        {
            var response = await _transactionService.UpdateTransaction(transactionId, model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPatch("ApproveTransaction/{transactionId}")]
        [Authorize(Policy = "Admin")]
        public async Task<ActionResult<ResponseView<TransactionViewModel>>> ApproveTransaction(Guid transactionId)
        {
            var response = await _transactionService.ApproveTransaction(transactionId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPatch("RejectTransaction/{transactionId}")]
        [Authorize(Policy = "Admin")]
        public async Task<ActionResult<ResponseView<TransactionViewModel>>> RejectTransaction(Guid transactionId)
        {
            var response = await _transactionService.RejectTransaction(transactionId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteTransaction/{transactionId}")]
        public async Task<ActionResult<ResponseView<TransactionViewModel>>> DeleteTransaction(Guid transactionId)
        {
            var response = await _transactionService.DeleteTransaction(transactionId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
