﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.Response;
using Application.ViewModels.TransactionCounterViewModels;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    [Authorize(Policy = "Admin")]
    public class TransactionCounterController : BaseController
    {
        private readonly ITransactionCounterService _transactionCounterService;
        private readonly IValidator<CreateTransactionCounterViewModel> _validator;
        public TransactionCounterController(ITransactionCounterService transactionCounterService, IValidator<CreateTransactionCounterViewModel> validator)
        {
            _transactionCounterService = transactionCounterService;
            _validator = validator;
        }

        [HttpPost("CreateTransactionCounter")]
        public async Task<ActionResult<ResponseView<TransactionCounterViewModel>>> CreateTransactionCounter(CreateTransactionCounterViewModel transactionCounterDTO)
        {
            var response = await _transactionCounterService.CreateTransactionCounter(transactionCounterDTO);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentTransactionCounters")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionCounterViewModel>>>> GetCurrentTransactionCounters(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionCounterService.GetCurrentTransactionCounters(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistorytTransactionCounters")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionCounterViewModel>>>> GetHistorytTransactionCounters(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionCounterService.GetHistoryTransactionCounters(pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTransactionCounterById/{transactionCounterId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<TransactionCounterViewModel>>> GetTransactionCounterById(Guid transactionCounterId)
        {
            var response = await _transactionCounterService.GetTransactionCounterById(transactionCounterId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetTransactionCounterByName/{transactionCounterName}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<Pagination<TransactionCounterViewModel>>>> GetTransactionCountersByName(string transactionCounterName, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _transactionCounterService.GetTransactionCountersByName(transactionCounterName, pageIndex: pageIndex, pageSize: pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateTransactionCounter/{transactionCounterId}")]
        public async Task<ActionResult<ResponseView<TransactionCounterViewModel>>> UpdateTransactionCounter(Guid transactionCounterId, CreateTransactionCounterViewModel transactionCounterDTO)
        {
            var response = await _transactionCounterService.UpdateTransactionCounter(transactionCounterId, transactionCounterDTO);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteTransactionCounter/{transactionCounterId}")]
        public async Task<ActionResult<ResponseView<TransactionCounterViewModel>>> DeleteTransactionCounter(Guid transactionCounterId)
        {
            var response = await _transactionCounterService.DeleteTransactionCounter(transactionCounterId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
