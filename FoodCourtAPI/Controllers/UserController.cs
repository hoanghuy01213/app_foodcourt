﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Application.ViewModels.UserViewModels;
using FluentValidation;
using Application.Interfaces;
using Application.Commons;
using Application.ViewModels.Response;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Azure.Core;

namespace FoodCourtAPI.Controllers
{
    [Authorize(Policy = "Admin")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("GetCurrentUser")]
        public async Task<ActionResult<ResponseView<Pagination<UserViewModel>>>> GetCurrentUser(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _userService.GetCurrentUserAsync(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryUser")]
        [Authorize(Policy = "Admin")]
        public async Task<ActionResult<ResponseView<Pagination<UserViewModel>>>> GetHistoryUser(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _userService.GetHistoryUser(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetUserById/{userId}")]
        public async Task<ActionResult<ResponseView<UserViewModel>>> GetUser(Guid userId)
        {
            var response = await _userService.GetUserByIdAsync(userId);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateUserById/{userId}")]
        public async Task<ActionResult<ResponseView<UserUpdateRequest>>> UpdateUser(Guid userId, UserUpdateRequest request)
        {
            var response = await _userService.UpdateUser(userId, request);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<bool>>> RegisterAsync(UserRegisterDTO registerUser)
        {
            var response = await _userService.RegisterAsync(registerUser);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<UserLoginResponse>>> LoginAsync(UserLoginRequest request)
        {
            var response = await _userService.LoginAsync(request);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("Change-Password/{UserId}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseView<ChangePasswordViewModel>>> ChangePassword(Guid UserId, [FromBody] ChangePasswordViewModel changePassword)
        {
            var response = await _userService.ChangePassword(UserId, changePassword);
            if (response.StatusCode is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
