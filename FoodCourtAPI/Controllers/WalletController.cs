﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.Response;
using Application.ViewModels.WalletViewModels;
using Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using System.Net;

namespace FoodCourtAPI.Controllers
{
    public class WalletController : BaseController
    {
        private readonly IWalletService _walletService;
        private readonly IValidator<CreateWalletViewModel> _createValidator;
        public WalletController(IWalletService walletService, IValidator<CreateWalletViewModel> createValidator)
        {
            _walletService = walletService;
            _createValidator = createValidator;
        }

        [HttpPost("CreateWallet")]
        public async Task<ActionResult<ResponseView<WalletViewModel>>> CreateWallet(CreateWalletViewModel walletDTO)
        {
            var response = await _walletService.CreateWallet(walletDTO);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetCurrentWallet")]
        public async Task<ActionResult<ResponseView<Pagination<WalletViewModel>>>> GetCurrentWallet(int pageIndex = 0, int pageSize = 10)
        {

            var response = await _walletService.GetCurrentWallet(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }


        [HttpGet("GetHistoryWallet")]
        public async Task<ActionResult<ResponseView<Pagination<WalletViewModel>>>> GetHistoryWallet(int pageIndex = 0, int pageSize = 10)
        {

            var response = await _walletService.GetHistoryWallet(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetActiveWallets")]
        public async Task<ActionResult<ResponseView<Pagination<WalletViewModel>>>> GetActiveWallets(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _walletService.GetActiveWallets(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetHistoryWalletsByCardId/{cardId}")]
        public async Task<ActionResult<ResponseView<Pagination<WalletViewModel>>>> GetHistoryWalletsByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _walletService.GetHistoryWalletsByCardId(cardId, pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetInActiveWallets")]
        public async Task<ActionResult<ResponseView<Pagination<WalletViewModel>>>> GetInActiveWallets(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _walletService.GetInActiveWallets(pageIndex, pageSize);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetWalletById/{walletId}")]
        public async Task<ActionResult<ResponseView<WalletViewModel>>> GetWalletById(Guid walletId)
        {
            var response = await _walletService.GetWalletById(walletId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("GetWalletsByCardId/{cardId}")]
        public async Task<ActionResult<ResponseView<Pagination<WalletViewModel>>>> GetWalletsByCardId(Guid cardId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _walletService.GetWalletsByCardId(cardId, pageIndex, pageSize); ;
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut("UpdateWallet/{walletId}")]
        public async Task<ActionResult<ResponseView<WalletViewModel>>> UpdateWallet(Guid walletId, CreateWalletViewModel model)
        {
            var response = await _walletService.UpdateWallet(walletId, model);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete("DeleteWallet/{walletId}")]
        public async Task<ActionResult<ResponseView<WalletViewModel>>> DeleteWallet(Guid walletId)
        {
            var response = await _walletService.DeleteWallet(walletId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPatch("ApprovedWallet/{walletId}")]
        public async Task<ActionResult<ResponseView<WalletViewModel>>> ApprovedWallet(Guid walletId)
        {
            var response = await _walletService.ApproveWallet(walletId);
            if (response.StatusCode is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
