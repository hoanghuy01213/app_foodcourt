﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.CardTypeViewModels;
using Application.ViewModels.CardViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.ShopCategoryViewModels;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.TransactionCounterViewModels;
using Application.ViewModels.TransactionViewModels;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.WalletViewModels;
using FluentValidation;
using FluentValidation.AspNetCore;
using FoodCourtAPI.Middlewares;
using FoodCourtAPI.Services;
using FoodCourtAPI.Validations;
using FoodCourtAPI.Validations.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Diagnostics;
using System.Text;
using System.Text.Json.Serialization;

namespace FoodCourtAPI
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services, AppConfiguration configuration)
        {
            services.AddControllers().AddJsonOptions(opt =>
            {
                opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                opt.JsonSerializerOptions.DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull;
            });
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddHealthChecks();
            services.AddSingleton<GlobalExceptionMiddleware>();
            services.AddSingleton<PerformaceMiddleware>();
            services.AddSingleton<Stopwatch>();
            services.AddScoped<IClaimService, ClaimsService>();
            services.AddHttpContextAccessor();
            services.AddFluentValidationAutoValidation();
            services.AddFluentValidationClientsideAdapters();

            //Validator
            services.AddScoped<IValidator<UserLoginRequest>, LoginValidation>();
            services.AddScoped<IValidator<UserRegisterDTO>, RegisterValidation>();
            services.AddScoped<IValidator<CreateShopViewModel>, CreateShopViewModelValidation>();
            services.AddScoped<IValidator<UpdateShopViewModel>, UpdateShopViewModelValidation>();
            services.AddScoped<IValidator<CreateTransactionCounterViewModel>, CreateTransactionCounterViewModelValidation>();
            services.AddScoped<IValidator<CreateShopCategoryViewModel>, CreateShopCategoryViewModelValidation>();
            services.AddScoped<IValidator<CreateCardTypeViewModel>, CreateCardTypeValidation>();
            services.AddScoped<IValidator<CreateProductViewModel>, CreateProductViewModelValidation>();
            services.AddScoped<IValidator<CreateCardViewModel>, CreateCardViewModelValidation>();
            services.AddScoped<IValidator<CreateWalletViewModel>, CreateWalletViewModelValidation>();
            services.AddScoped<IValidator<CreateOrderViewModel>, CreateOrderViewModelValidation>();
            services.AddScoped<IValidator<CreateTransactionViewModel>, CreateTransactionViewModelValidation>();
            services.AddScoped<IValidator<UserUpdateRequest>, UpdateUserValidation>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Food Court",
                    Version = "v1",
                    Description = "This is the API of Toan",
                    Contact = new OpenApiContact
                    {
                        Url = new Uri("https://google.com")
                    }
                });
                var securityScheme = new OpenApiSecurityScheme
                {
                    BearerFormat = "JWT",
                    Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    Scheme = "Bearer",

                    Reference = new OpenApiReference
                    {
                        Id = JwtBearerDefaults.AuthenticationScheme,
                        Type = ReferenceType.SecurityScheme
                    }
                };
                c.AddSecurityDefinition(securityScheme.Reference.Id, securityScheme);
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { securityScheme, Array.Empty<string>() }
                });
            });

            // JWT
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = configuration.JWT.Issuer,
                    ValidAudience = configuration.JWT.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.JWT.Key)),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true
                };
            });
            services.AddAuthorization(x =>
            {
                x.AddPolicy("Admin", p => p.RequireRole("Admin"));
                x.AddPolicy("User", p => p.RequireRole("User"));
                x.AddPolicy("Both", p => p.RequireRole("Admin,User"));
            });
            return services;
        }
    }
}
