﻿using Infrastructures;
using FoodCourtAPI.Middlewares;
using FoodCourtAPI;
using Application.Commons;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);

// parse the configuration in appsettings
var configuration = builder.Configuration.Get<AppConfiguration>();
builder.Services.AddInfrastructuresService(configuration)
                .AddWebAPIService(configuration);
builder.Services.AddSingleton(configuration);
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins("*")
                          .AllowAnyHeader()
                          .AllowAnyMethod();
                      });
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FoodCourt v1"));
}

app.UseSwagger();
app.UseSwaggerUI();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FoodCourt v1"));

app.UseMiddleware<GlobalExceptionMiddleware>();
app.UseMiddleware<PerformaceMiddleware>();
app.MapHealthChecks("/healthchecks");
app.UseHttpsRedirection();
app.UseAuthorization();
app.UseCors(MyAllowSpecificOrigins);
app.MapControllers();
app.Run();
