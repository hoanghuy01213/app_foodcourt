﻿using Application.ViewModels.CardTypeViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateCardTypeValidation : AbstractValidator<CreateCardTypeViewModel>
    {
        public CreateCardTypeValidation() 
        {
            RuleFor(x => x.Points).NotNull().NotEmpty().GreaterThan(0).WithMessage("Points is required");
            RuleFor(x => x.Discount).NotNull().NotEmpty().WithMessage("Discount is required");
            RuleFor(x => x.Avatar).NotNull().NotEmpty().WithMessage("Avatar is required");
        }
    }
}
