﻿using Application.Interfaces;
using Application.ViewModels.CardViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateCardViewModelValidation : AbstractValidator<CreateCardViewModel>
    {
        public CreateCardViewModelValidation()
        {
            RuleFor(x => x.CardNumber).NotEmpty().WithMessage("CardNumber is Required");
            RuleFor(x => x.SecurityCode).NotEmpty().WithMessage("SecurityCode is Required");
            RuleFor(x => x.CardTypeId).NotEmpty().WithMessage("CardTypeId is Required");
        }
    }
}
