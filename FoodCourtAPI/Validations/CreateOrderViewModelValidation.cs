﻿using Application;
using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using FluentValidation;
using NuGet.Protocol;

namespace FoodCourtAPI.Validations
{
    public class CreateOrderViewModelValidation : AbstractValidator<CreateOrderViewModel>
    {
        public CreateOrderViewModelValidation() 
        {
            RuleFor(x => x.CardId).NotEmpty().NotEqual(Guid.Empty).WithMessage("Card Id is Required").WithMessage("Invalid Card Id");
        }
    }
}
