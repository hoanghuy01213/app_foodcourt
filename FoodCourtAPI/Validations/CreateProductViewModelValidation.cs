﻿using Application.Interfaces;
using Application.ViewModels.ProductViewModels;
using Domain.Entities;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateProductViewModelValidation : AbstractValidator<CreateProductViewModel>
    {
        public CreateProductViewModelValidation() 
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().WithMessage("Product Name is Required");
            RuleFor(x => x.Price).NotNull().NotEmpty().GreaterThan(0).WithMessage("Price Must Greater Than '0'");
            RuleFor(x => x.UrlImage).NotNull().NotEmpty().WithMessage("UrlImage is required");
        }
    }
}
