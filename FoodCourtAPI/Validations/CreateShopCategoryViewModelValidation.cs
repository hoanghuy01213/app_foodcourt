﻿using Application.Interfaces;
using Application.ViewModels.ShopCategoryViewModels;
using Domain.Entities;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateShopCategoryViewModelValidation : AbstractValidator<CreateShopCategoryViewModel>
    {
        public CreateShopCategoryViewModelValidation()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Please Input Information");
            RuleFor(x => x.ShopId)
                .NotEqual(Guid.Empty)
                .NotNull()
                .WithMessage("Please Input Information");
        }
    }
}
