﻿using Application.Interfaces;
using Application.IRepositories;
using Application.Services;
using Application.ViewModels.ShopsViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateShopViewModelValidation : AbstractValidator<CreateShopViewModel>
    {
        public CreateShopViewModelValidation() 
        {
            RuleFor(x => x.ShopName).NotNull().NotEmpty().WithMessage("Invalid Shop Name  ");
            RuleFor(x => x.UserId).NotEmpty().NotEqual(Guid.Empty).WithMessage("In valid UserId");
            RuleFor(x => x.Location).NotNull().NotEmpty().WithMessage("Location is Required");
        }
    }
}
