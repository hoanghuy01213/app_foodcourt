﻿using Application.ViewModels.TransactionCounterViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateTransactionCounterViewModelValidation : AbstractValidator<CreateTransactionCounterViewModel>
    {
        public CreateTransactionCounterViewModelValidation()
        {
            RuleFor(x => x.TransactionCounterName).NotEmpty().WithMessage("TransactionCounterName is Required");
            RuleFor(x => x.Location).NotEmpty().WithMessage("Location is Required");
        }
    }
}
