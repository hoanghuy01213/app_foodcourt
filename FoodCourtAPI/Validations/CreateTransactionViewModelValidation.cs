﻿using Application.ViewModels.TransactionViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateTransactionViewModelValidation : AbstractValidator<CreateTransactionViewModel>
    {
        public CreateTransactionViewModelValidation() 
        {
            RuleFor(x => x.Money).NotEmpty().GreaterThan(0).WithMessage("Money is Required");
        }
    }
}
