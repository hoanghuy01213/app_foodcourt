﻿using Application.ViewModels.WalletViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class CreateWalletViewModelValidation : AbstractValidator<CreateWalletViewModel>
    {
        public CreateWalletViewModelValidation() 
        {
            RuleFor(x => x.Remainder).NotEmpty().WithMessage("Remainder is Required");
        }
    }
}
