﻿using Application.Interfaces;
using Application.ViewModels.ShopsViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations
{
    public class UpdateShopViewModelValidation : AbstractValidator<UpdateShopViewModel>
    {
        public UpdateShopViewModelValidation()
        {
            RuleFor(x => x.ShopName).NotNull().NotEmpty().WithMessage("ShopName is Required");
            RuleFor(x => x.Location).NotNull().NotEmpty().WithMessage("Location is Required");
        }
    }
}
