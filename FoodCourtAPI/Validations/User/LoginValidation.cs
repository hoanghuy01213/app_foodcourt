﻿using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations.User
{
    public class LoginValidation : AbstractValidator<UserLoginRequest>
    {
        public LoginValidation()
        {
            RuleFor(x => x.UserName).NotEmpty()
                                  .MinimumLength(5)
                                  .MaximumLength(20)
                                  .Unless(HasEmail)
                                  .Must(username => false)
                                  .When(HasBothUsernameAndEmail, ApplyConditionTo.CurrentValidator)
                                  .WithMessage("Invalid request.");
            RuleFor(x => x.Email)
                                   .MinimumLength(10)
                                   .MaximumLength(50)
                                   .EmailAddress()
                                   .NotEmpty()
                                   .Unless(HasUsername)
                                   .Must(email => false)
                                   .When(HasBothUsernameAndEmail, ApplyConditionTo.CurrentValidator)
                                   .WithMessage("Invalid request.");
            RuleFor(x => x.Password).MinimumLength(0).MaximumLength(24).NotEmpty().WithMessage("Invalid Password");
        }
        private bool HasUsername(UserLoginRequest req) => !string.IsNullOrEmpty(req.UserName);
        private bool HasEmail(UserLoginRequest req) => !string.IsNullOrEmpty(req.Email);
        private bool HasBothUsernameAndEmail(UserLoginRequest req)
            => !string.IsNullOrEmpty(req.UserName) && !string.IsNullOrEmpty(req.Email);
    }
}
