﻿using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace FoodCourtAPI.Validations.User
{
    public class RegisterValidation:AbstractValidator<UserRegisterDTO>
    {
        public RegisterValidation()
        {
            RuleFor(x => x.Username).NotEmpty()
                                    .NotNull()
                                    .WithMessage("User Name cannot be Null or Empty!!!");
            RuleFor(x => x.Password).NotEmpty()
                                    .NotNull()
                                    .WithMessage("Password cannot be Null or Empty!!!");
            RuleFor(x => x.Email).NotEmpty()
                                 .NotNull()
                                 .EmailAddress()
                                 .WithMessage("Invalid Email!");
        }
    }
}
