﻿using Application.ViewModels.UserViewModels;
using FluentValidation;
using System.Data;

namespace FoodCourtAPI.Validations.User
{
    public class UpdateUserValidation : AbstractValidator<UserUpdateRequest>
    {
        public UpdateUserValidation()
        {
            RuleFor(x => x.UserName)
                .NotEmpty()
                .WithMessage("UserName should not be empty");
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email should not be empty")
                .EmailAddress()
                .WithMessage("Invalid Email Format");
        }
    }
}
