﻿using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Application;
using Infrastructures.Mappers;
using Infrastructures.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using Application.IRepositories;
using Application.Commons;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualBasic.FileIO;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services, AppConfiguration configuration)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<ICurrentTimeService, CurrentTime>();
            //Repository
            services.AddScoped<IShopRepository, ShopRepository>();
            services.AddScoped<IShopCategoryRepository, ShopCategoryRepository>();
            services.AddScoped<ITransactionCounterRepository, TransactionCounterRepository>();
            services.AddScoped<ICardTypeRepository, CardTypeRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICardRepository, CardRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderDetailRepository, OrderDetailRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            //Services
            services.AddScoped<IShopService, ShopService>();
            services.AddScoped<IShopCategoryService, ShopCategoryService>();
            services.AddScoped<ITransactionCounterService, TransactionCounterService>();
            services.AddScoped<ICardTypeService, CardTypeService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICardService, CardService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<ICurrentTimeService, CurrentTime>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderDetailService, OrderDetailService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<ISaleReportService, SaleReportService>();
            services.AddScoped<IJwtService, JwtService>();
            // ATTENTION: if you do migration please check file README.md
            services.AddDbContext<AppDbContext>(options =>
            {
                // Configure your database provider and connection string here
                options.UseSqlServer(configuration.DatabaseConnection);
                //options.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));
                //options.EnableSensitiveDataLogging();
            });


            // this configuration just use in-memory for fast develop
            //services.AddDbContext<AppDbContext>(option => option.UseInMemoryDatabase("test"));

            services.AddAutoMapper(typeof(MapperConfigurationsProfile).Assembly);
            //services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            return services;
        }
    }
}
