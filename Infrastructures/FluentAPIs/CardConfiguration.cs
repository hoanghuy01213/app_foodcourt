﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class CardConfiguration : BaseEntityConfiguration<Card>
    {
        public override void Configure(EntityTypeBuilder<Card> builder)
        {
            base.Configure(builder);
            builder.HasOne(x => x.CardType).WithMany(x => x.Cards).HasForeignKey(x => x.CardTypeId);
            builder.HasMany(x=>x.Orders).WithOne(x=>x.Card).HasForeignKey(x => x.CardId);
            builder.HasData(
                new Card
                {
                    Id = new Guid("4DE04F4C-DF05-4CBD-BB17-2F742933B83C"),
                    CardNumber= "123456789",
                    SecurityCode= "354300",
                    ExpirationDate= DateTime.Now.AddDays(365),
                    CardTypeId= new Guid("932E7F21-7715-40E0-A14A-BD417E4283DE"),
                    CreationDate =DateTime.Now
                }); 
        }
    }
}
