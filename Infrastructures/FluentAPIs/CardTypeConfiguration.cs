﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class CardTypeConfiguration : BaseEntityConfiguration<CardType>
    {
        public override void Configure(EntityTypeBuilder<CardType> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => x.Id);
            builder.HasData(
            new CardType
            {
                Id = new Guid("932E7F21-7715-40E0-A14A-BD417E4283DE"),
                Avatar = "Dog",
                Points = 5,
                Discount = 5,
                CreationDate = DateTime.Now
            });
        }
    }
}
