﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class ProductConfiguration : BaseEntityConfiguration<Product>
    {
        public override void Configure(EntityTypeBuilder<Product> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.ShopCategory).WithMany(x => x.Products).HasForeignKey(x => x.ShopCategoryId);
            builder.HasData(
                new Product
                {
                    Id = new Guid("00000001-0000-0000-0000-000000000000"),
                    Name="Hamburger",
                    Price=50000,
                    UrlImage="gooogle.com",
                    ShopCategoryId=new Guid("4289EB61-8035-4696-A182-8BA3F5E51E6B"),
                    CreationDate=DateTime.Now
                });
        }
    }
}
