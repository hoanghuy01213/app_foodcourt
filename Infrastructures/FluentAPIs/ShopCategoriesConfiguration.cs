﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class ShopCategoriesConfiguration : BaseEntityConfiguration<ShopCategory>
    {
        public override void Configure(EntityTypeBuilder<ShopCategory> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Shops).WithMany(x => x.ShopCategories).HasForeignKey(x => x.ShopId);
            builder.HasData(
                new ShopCategory
                {
                    Id = new Guid("4289EB61-8035-4696-A182-8BA3F5E51E6B"),
                    ShopId = new Guid("680617AF-8F48-42A1-912E-8F55CA8E0AAE"),
                    Name = "Food",
                    CreationDate = DateTime.Now
                });
        }
    }
}
