﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class ShopConfiguration : BaseEntityConfiguration<Shop>
    {
        public override void Configure(EntityTypeBuilder<Shop> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => x.Id);
            builder.HasData(
                new Shop
                {
                    Id= new Guid("680617AF-8F48-42A1-912E-8F55CA8E0AAE"),
                    ShopName="Jolibee",
                    Location="Australia",
                    UserId=new Guid("00000001-0000-0000-0000-000000000000"),
                    CreationDate=DateTime.Now
                });
        }
    }
}
