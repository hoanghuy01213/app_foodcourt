﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class TransactionConfiguration : BaseEntityConfiguration<Transaction>
    {
        public override void Configure(EntityTypeBuilder<Transaction> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.TransactionCounter).WithMany(x => x.Transactions).HasForeignKey(x => x.TransactionCounterId);
            builder.HasOne(x => x.Wallet).WithMany(x => x.Transactions).HasForeignKey(x => x.WalletId).OnDelete(DeleteBehavior.ClientNoAction);
            builder.HasOne(x => x.Orders).WithMany(x => x.Transactions).HasForeignKey(x => x.OrderId);
        }
    }
}
