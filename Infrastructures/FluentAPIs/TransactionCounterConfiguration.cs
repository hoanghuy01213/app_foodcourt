﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class TransactionCounterConfiguration : BaseEntityConfiguration<TransactionCounter>
    {
        public override void Configure(EntityTypeBuilder<TransactionCounter> builder)
        {
            base.Configure(builder);
            builder.HasKey(t => t.Id);
        }
    }
}
