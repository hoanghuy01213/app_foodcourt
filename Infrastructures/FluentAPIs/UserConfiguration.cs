﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.Shops).WithOne(x => x.User).HasForeignKey(x => x.UserId);
            builder.HasData(new User
            {
                Id = new Guid("00000001-0000-0000-0000-000000000000"),
                UserName = "toannm7",
                /*123*/
                HashedPassword = "$2a$11$/4Me1nFnejtO5Zm0P.Dc1OZJNxeI09qngV595be8xTizDo/mne3i6",
                Email = "toanmnh2002@gmail.com",
                CreditBalance = 10000,
                Point = 5,
                CreationDate = DateTime.Now,
                IsDeleted = false,
                Role = UserRoleEnum.Admin,
            }, new User
            {
                Id = new Guid("00000002-0000-0000-0000-000000000000"),
                UserName = "HuyNH",
                /*123*/
                HashedPassword = "$2a$11$/4Me1nFnejtO5Zm0P.Dc1OZJNxeI09qngV595be8xTizDo/mne3i6",
                Email = "huyNH@gmail.com",
                CreationDate = DateTime.Now,
                CreditBalance = 10000,
                Point = 5,
                IsDeleted = false,
                Role = UserRoleEnum.User,
            });
        }
    }
}

