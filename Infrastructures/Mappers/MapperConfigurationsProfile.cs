﻿using Application.Commons;
using Application.Utils;
using Application.ViewModels.CardTypeViewModels;
using Application.ViewModels.CardViewModels;
using Application.ViewModels.OrderDetailViewModels;
using Application.ViewModels.OrderViewModels;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.ShopCategoryViewModels;
using Application.ViewModels.ShopsViewModels;
using Application.ViewModels.ShopViewModels;
using Application.ViewModels.TransactionCounterViewModels;
using Application.ViewModels.TransactionViewModels;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.WalletViewModels;
using AutoMapper;
using Domain.Entities;
using static Application.ViewModels.ShopViewModels.ShopViewIncludeModel;

namespace Infrastructures.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {
        public MapperConfigurationsProfile()
        {
            //Pagination
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            //Shop
            CreateMap<Shop, ShopViewModel>().ForMember(d => d.ShopId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<Shop, ShopViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<Shop, ShopViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<Shop, ShopViewModel>>())
                .ReverseMap();
            CreateMap<CreateShopViewModel, Shop>().ReverseMap();
            CreateMap<UpdateShopViewModel, Shop>().ReverseMap();
            CreateMap<Shop, ShopResponseViewModel>()
                .ForMember(d => d.ShopId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<Shop, ShopResponseViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<Shop, ShopResponseViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<Shop, ShopResponseViewModel>>())
                .ReverseMap();

            //ShopInclude

            CreateMap<UserViewIncludeModel, User>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.UserId))
                .ForMember(d => d.Shops, s => s.MapFrom(x => x.Shop))
                .ReverseMap();
                
            //_OrderDetail
            CreateMap<ViewOrderDetail, OrderDetail>()
                 .ForMember(d => d.OrderId, s => s.MapFrom(x => x.OrderId))
                 .ReverseMap();

            //_Product
            CreateMap<ViewProduct, Product>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.ProductId))
                .ForMember(d => d.OrderDetails, s => s.MapFrom(x => x.ViewOrderDetail))
                .ReverseMap();

            CreateMap<ProductShop, Product>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.ProductId))
                .ReverseMap();

            CreateMap<ShopProductTransaction, Product>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.ProductId))
                .ForMember(d => d.OrderDetails, s => s.MapFrom(s => s.ShopProductTransactionOrderDetails))
                .ReverseMap();

            //_ShopCategory
            CreateMap<ViewShopCategory, ShopCategory>()
                .ForMember(d => d.Products, s => s.MapFrom(x => x.ViewProduct))
                .ForMember(d => d.Id, s => s.MapFrom(x => x.ShopCategoryId))
                .ReverseMap();

            CreateMap<ShopCategoryProduct, ShopCategory>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.ShopCategoryId))
                .ForMember(d => d.Products, s => s.MapFrom(x => x.ProductShop))
                .ReverseMap();

            CreateMap<ShopCategoryTransaction, ShopCategory>()
               .ForMember(d => d.Id, s => s.MapFrom(x => x.ShopCategoryId))
               .ForMember(d => d.Products, s => s.MapFrom(x => x.ShopProductTransaction))
               .ReverseMap();

            //_Order
            CreateMap<Order, OrderEntity>()
                .ForMember(d => d.OrderId, s => s.MapFrom(x => x.Id))
                .ReverseMap();
            CreateMap<ShopProductTransactionOrderDetail, OrderDetail>()
               .ForMember(d => d.Orders, s => s.MapFrom(x => x.Order))
               .ReverseMap();
            //_Transaction
            CreateMap<Transaction, MyTransaction>()
                .ForMember(d => d.TransactionId, s => s.MapFrom(x => x.Id))
                .ReverseMap();

            CreateMap<Order, OrderTransaction>()
               .ForMember(d => d.Transaction, s => s.MapFrom(x => x.Transactions))
               .ReverseMap();
            //_ShopMain
            CreateMap<Shop, ShopViewIncludeModel>()
                .ForMember(d => d.ShopId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.ViewShopCategory, s => s.MapFrom(x => x.ShopCategories))
                .ReverseMap();

            CreateMap<Shop, ShopViewProductViewModel>()
                .ForMember(d => d.ShopId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.ShopCategoryProduct, s => s.MapFrom(x => x.ShopCategories))
                .ReverseMap();

            CreateMap<Shop, ShopViewTransactionViewModel>()
                .ForMember(d => d.ShopId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.ShopCategoryTransaction, s => s.MapFrom(x => x.ShopCategories))
                .ReverseMap();

            //User
            CreateMap<User, UserLoginRequest>()
                    .ForMember(d => d.Password, s => s.MapFrom(x => x.HashedPassword))
                    .ReverseMap();
            CreateMap<UserLoginResponse, User>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.UserId))
                .ForMember(d => d.Role, s => s.MapFrom(x => x.Role.ToString())).ReverseMap();
            CreateMap<UserRegisterDTO, User>().ForMember(d => d.HashedPassword, s => s.MapFrom(x => x.Password.Hash())).ReverseMap();
            CreateMap<UserViewModel, User>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.UserId))
                .ReverseMap()
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<User, UserViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<User, UserViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<User, UserViewModel>>());
            CreateMap<UserUpdateRequest, User>().ReverseMap();

            //Shop category
            CreateMap<CreateShopCategoryViewModel, ShopCategory>().ReverseMap();
            CreateMap<ShopCategoryViewModel, ShopCategory>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.ShopCategoryId))
                .ReverseMap()
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<ShopCategory, ShopCategoryViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<ShopCategory, ShopCategoryViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<ShopCategory, ShopCategoryViewModel>>());

            CreateMap<ShopCategories, ShopCategory>().ReverseMap();
            //Transaction Counter
            CreateMap<TransactionCounterViewModel, TransactionCounter>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.TransactionCounterId))
                .ReverseMap()
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<TransactionCounter, TransactionCounterViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<TransactionCounter, TransactionCounterViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<TransactionCounter, TransactionCounterViewModel>>());
            CreateMap<CreateTransactionCounterViewModel, TransactionCounter>().ReverseMap();

            //Card Type
            CreateMap<CardTypeViewModel, CardType>().ReverseMap();
            CreateMap<CreateCardTypeViewModel, CardType>().ReverseMap();
            CreateMap<CardType, CardTypeViewModel>()
                .ForMember(d => d.CardTypeId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<CardType, CardTypeViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<CardType, CardTypeViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<CardType, CardTypeViewModel>>())
                .ReverseMap();
            ;

            //Product
            CreateMap<ProductViewModel, Product>()
                .ReverseMap()
                .ForMember(d => d.ProductId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<Product, ProductViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<Product, ProductViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<Product, ProductViewModel>>());
            CreateMap<CreateProductViewModel, Product>().ReverseMap();

            //Card
            CreateMap<Card, CardViewModel>()
                .ForMember(d => d.CreatedByEmail, s => s.MapFrom<ResolverMappingGeneric<Card, CardViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<Card, CardViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<Card, CardViewModel>>())
                .ReverseMap();
            CreateMap<CreateCardViewModel, Card>().ReverseMap();
            CreateMap<UpdateCardViewModel, Card>().ReverseMap();

            //Wallet
            CreateMap<WalletViewModel, Wallet>().ReverseMap();
            CreateMap<CreateWalletViewModel, Wallet>().ReverseMap();
            CreateMap<Wallet, WalletPermission>().ReverseMap();

            //Order
            CreateMap<OrderViewModel, Order>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.OrderId))
                .ReverseMap()
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<Order, OrderViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<Order, OrderViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<Order, OrderViewModel>>())
                ;
            CreateMap<CreateOrderViewModel, Order>().ReverseMap();
            CreateMap<ApproveOrderViewModel, Order>().ReverseMap();
            CreateMap<Order, OrderResponseViewModel>()
                .ForMember(d => d.OrderId, s => s.MapFrom(x => x.Id))
                .ReverseMap();
            CreateMap<OrderDetails, OrderDetail>().ReverseMap();
            CreateMap<UpdateOrderViewModel, Order>().ReverseMap();

            //OrderDetail
            CreateMap<AddOrderDetailViewModel, OrderDetail>().ReverseMap();
            CreateMap<CreateOrderDetailViewModel, OrderDetail>().ReverseMap();
            CreateMap<UpdateOrderDetailViewModel, OrderDetail>().ReverseMap();

            //Transaction
            CreateMap<TransactionViewModel, Transaction>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.TransactionId))
                .ReverseMap()
                .ForMember(d => d.CreatedBy, s => s.MapFrom<ResolverMappingGeneric<Transaction, TransactionViewModel>>())
                .ForMember(d => d.ModificatedBy, s => s.MapFrom<ResolverMappingGeneric<Transaction, TransactionViewModel>>())
                .ForMember(d => d.DeletedBy, s => s.MapFrom<ResolverMappingGeneric<Transaction, TransactionViewModel>>());
            CreateMap<CreateTransactionViewModel, Transaction>().ReverseMap();
            CreateMap<ApproveTransactionViewModel, Transaction>().ReverseMap();
        }
    }
}
