﻿using Application.ViewModels.CardViewModels;
using Application;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Mappers
{
    public class ResolverMappingGeneric<TSource, TDestination> : IValueResolver<TSource, TDestination, string>
    where TSource : BaseEntity
    where TDestination : class
    {
        private readonly IUnitOfWork _unitOfWork;

        public ResolverMappingGeneric(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public string Resolve(TSource source, TDestination destination, string destMember, ResolutionContext context)
        {
            if (source.CreatedBy == Guid.Empty || source.DeletedBy == Guid.Empty || source.ModificatedBy == Guid.Empty)
                return null;

            var user = _unitOfWork.UserRepository.FirstOrDefaultAsync(x =>
                x.Id == source.CreatedBy || x.Id == source.DeletedBy || x.Id == source.ModificatedBy).Result;

            if (user is null)
                return null;
            return user.UserName;
        }
    }
}
