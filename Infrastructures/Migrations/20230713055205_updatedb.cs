﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class updatedb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Card",
                keyColumn: "Id",
                keyValue: new Guid("4de04f4c-df05-4cbd-bb17-2f742933b83c"),
                columns: new[] { "CreationDate", "ExpirationDate" },
                values: new object[] { new DateTime(2023, 7, 13, 12, 52, 4, 471, DateTimeKind.Local).AddTicks(1937), new DateTime(2024, 7, 12, 12, 52, 4, 471, DateTimeKind.Local).AddTicks(1912) });

            migrationBuilder.UpdateData(
                table: "CardType",
                keyColumn: "Id",
                keyValue: new Guid("932e7f21-7715-40e0-a14a-bd417e4283de"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 12, 52, 4, 471, DateTimeKind.Local).AddTicks(9533));

            migrationBuilder.UpdateData(
                table: "Product",
                keyColumn: "Id",
                keyValue: new Guid("00000001-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 12, 52, 4, 472, DateTimeKind.Local).AddTicks(9111));

            migrationBuilder.UpdateData(
                table: "Shop",
                keyColumn: "Id",
                keyValue: new Guid("680617af-8f48-42a1-912e-8f55ca8e0aae"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 12, 52, 4, 473, DateTimeKind.Local).AddTicks(2693));

            migrationBuilder.UpdateData(
                table: "ShopCategory",
                keyColumn: "Id",
                keyValue: new Guid("4289eb61-8035-4696-a182-8ba3f5e51e6b"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 12, 52, 4, 473, DateTimeKind.Local).AddTicks(1267));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000001-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 12, 52, 4, 474, DateTimeKind.Local).AddTicks(676));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000002-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 12, 52, 4, 474, DateTimeKind.Local).AddTicks(684));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Card",
                keyColumn: "Id",
                keyValue: new Guid("4de04f4c-df05-4cbd-bb17-2f742933b83c"),
                columns: new[] { "CreationDate", "ExpirationDate" },
                values: new object[] { new DateTime(2023, 7, 13, 1, 50, 41, 588, DateTimeKind.Local).AddTicks(3270), new DateTime(2024, 7, 12, 1, 50, 41, 588, DateTimeKind.Local).AddTicks(3243) });

            migrationBuilder.UpdateData(
                table: "CardType",
                keyColumn: "Id",
                keyValue: new Guid("932e7f21-7715-40e0-a14a-bd417e4283de"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 1, 50, 41, 589, DateTimeKind.Local).AddTicks(1628));

            migrationBuilder.UpdateData(
                table: "Product",
                keyColumn: "Id",
                keyValue: new Guid("00000001-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 1, 50, 41, 590, DateTimeKind.Local).AddTicks(1603));

            migrationBuilder.UpdateData(
                table: "Shop",
                keyColumn: "Id",
                keyValue: new Guid("680617af-8f48-42a1-912e-8f55ca8e0aae"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 1, 50, 41, 590, DateTimeKind.Local).AddTicks(5435));

            migrationBuilder.UpdateData(
                table: "ShopCategory",
                keyColumn: "Id",
                keyValue: new Guid("4289eb61-8035-4696-a182-8ba3f5e51e6b"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 1, 50, 41, 590, DateTimeKind.Local).AddTicks(4018));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000001-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 1, 50, 41, 591, DateTimeKind.Local).AddTicks(3600));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000002-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2023, 7, 13, 1, 50, 41, 591, DateTimeKind.Local).AddTicks(3608));
        }
    }
}
