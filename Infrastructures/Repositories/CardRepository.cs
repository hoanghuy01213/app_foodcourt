﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructures.Repositories
{
    public class CardRepository : GenericRepository<Card>, ICardRepository
    {
        public CardRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}
