﻿using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;

namespace Infrastructures.Repositories
{
    public class CardTypeRepository : GenericRepository<CardType>, ICardTypeRepository
    {
        public CardTypeRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}
