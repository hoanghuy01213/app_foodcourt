﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly DbSet<T> _dbSet;
        private ICurrentTimeService _currentTime;
        private IClaimService _claimService;
        public GenericRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService)
        {
            _dbSet = appDBContext.Set<T>();
            _currentTime = currentTime;
            _claimService = claimService;
        }
        public async Task AddEntityAsync(T entity)
        {
            entity.CreationDate = _currentTime.GetCurrentTime();
            entity.CreatedBy = _claimService.GetCurrentUserId;
            await _dbSet.AddAsync(entity);
        }
        public async Task AddEntityRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreationDate = _currentTime.GetCurrentTime();
                entity.CreatedBy = _claimService.GetCurrentUserId;
            }
            await _dbSet.AddRangeAsync(entities);
        }
        public void UpdateEntity(T entity)
        {
            entity.ModificationDate = _currentTime.GetCurrentTime();
            entity.ModificatedBy = _claimService.GetCurrentUserId;
            _dbSet.Update(entity);
        }
        public void UpdateRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.ModificationDate = _currentTime.GetCurrentTime();
                entity.ModificatedBy = _claimService.GetCurrentUserId;
            }
            _dbSet.UpdateRange(entities);
        }
        public void SoftRemoveEntity(T entity)
        {
            entity.IsDeleted = true;
            entity.DeletionDate = _currentTime.GetCurrentTime();
            entity.DeletedBy = _claimService.GetCurrentUserId;
            _dbSet.Update(entity);
        }
        public void SoftRemoveEntityById(T entity)
        {
            _dbSet.Remove(entity);
        }
        public void SoftRemoveEntityRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
                entity.DeletionDate = _currentTime.GetCurrentTime();
                entity.DeletedBy = _claimService.GetCurrentUserId;
            }
            _dbSet.UpdateRange(entities);
        }
        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression) => await _dbSet.Where(expression).ToListAsync();
        public async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>>? expression = null, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            IQueryable<T> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            if (expression is not null)
            {
                query = query.Where(expression);
            }
            return await query.ToListAsync();
        }
        #region ToPagination
        public async Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };
            return result;
        }
        public async Task<Pagination<T>> ToPagination(Expression<Func<T, bool>> expression,
                                                        int pageIndex = 0,
                                                        int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(expression)
                                    .OrderByDescending(e => e.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };
            return result;
        }
        public async Task<Pagination<T>> ToPagination(Expression<Func<T, bool>> expression = null,
            Func<IQueryable<T>, IQueryable<T>> include = null,
            int pageIndex = 0,
            int pageSize = 10,
            Expression<Func<T, object>> sortColumn = null,
            SortDirection sortDirection = SortDirection.Descending)
        {
            var query = _dbSet.AsQueryable();

            // Include related entities if specified
            if (include is not null)
            {
                query = include(query);
            }

            // Apply filtering if specified
            if (expression is not null)
            {
                query = query.Where(expression);
            }

            // Apply sorting if specified
            if (sortColumn is not null)
            {
                query = sortDirection == SortDirection.Ascending
                    ? query.OrderBy(sortColumn)
                    : query.OrderByDescending(sortColumn);
            }

            // Calculate the total item count for the query
            var itemCount = await query.CountAsync();

            // Apply pagination to the query
            var items = await query.Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();

            // Create the pagination result
            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };

            return result;
        }
        public async Task<Pagination<T>> ToPaginationNoQueryFilter(Expression<Func<T, bool>> expression = null,
            Func<IQueryable<T>, IQueryable<T>> include = null,
            int pageIndex = 0,
            int pageSize = 10,
            Expression<Func<T, object>> sortColumn = null,
            SortDirection sortDirection = SortDirection.Descending)
        {
            var query = _dbSet.AsQueryable().IgnoreQueryFilters();

            // Include related entities if specified
            if (include is not null)
            {
                query = include(query);
            }

            // Apply filtering if specified
            if (expression is not null)
            {
                query = query.Where(expression);
            }

            // Apply sorting if specified
            if (sortColumn is not null)
            {
                query = sortDirection == SortDirection.Ascending
                    ? query.OrderBy(sortColumn)
                    : query.OrderByDescending(sortColumn);
            }

            // Calculate the total item count for the query
            var itemCount = await query.CountAsync();

            // Apply pagination to the query
            var items = await query.Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();

            // Create the pagination result
            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };

            return result;
        }
        public async Task<Pagination<T>> ToPagination(
            Expression<Func<T, bool>> expression = null,
            Func<IQueryable<T>, IQueryable<T>> include = null,
            int pageIndex = 0,
            int pageSize = 10)
        {
            var query = _dbSet.AsQueryable();

            if (include is not null)
            {
                query = include(query);
            }

            if (expression is not null)
            {
                query = query.Where(expression);
            }

            var itemCount = await query.CountAsync();

            var items = await query.OrderByDescending(e => e.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };

            return result;
        }
        #endregion
        #region GetById
        public async Task<T?> GetEntityByIdAsync(Guid id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<T?> FirstOrDefaultAsync(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            IQueryable<T> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            return await query.AsNoTracking().IgnoreQueryFilters().FirstOrDefaultAsync(expression);
        }
        public async Task<bool> CheckExistGeneric(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            IQueryable<T> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            return await query.AnyAsync(expression);
        }
        public async Task<T?> GetEntityByGenericAsync(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            IQueryable<T> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            return await query.FirstOrDefaultAsync(expression);
        }
        #endregion
    }
}
