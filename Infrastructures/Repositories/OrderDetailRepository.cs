﻿using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>,IOrderDetailRepository
    {
        public OrderDetailRepository(
            AppDbContext appDBContext,
            ICurrentTimeService currentTime,
            IClaimService claimService
            ) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}
