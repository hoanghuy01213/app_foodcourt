﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext appDBContext,
            ICurrentTimeService currentTime,
            IClaimService claimService
            ) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}
