﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructures.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly AppDbContext _dbContext;

        public ProductRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService, AppDbContext dbContext) : base(appDBContext, currentTime, claimService)
        {
            _dbContext = dbContext;
        }


        public async Task<Pagination<Product>> GetProductsByName(string productName, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Product.CountAsync();
            var items = await _dbContext.Product.Where(x => x.Name.Contains(productName))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();
            var result = new Pagination<Product>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items
            };
            return result;
        }

        public async Task<Pagination<Product>> GetProductsByShopCategoryId(Guid shopCategoryId, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Product.CountAsync();
            var items = await _dbContext.Product.Where(x => x.ShopCategoryId.Equals(shopCategoryId))
                                        .OrderByDescending(x => x.CreationDate)
                                        .Skip(pageIndex * pageSize)
                                        .Take(pageSize)
                                        .AsNoTracking()
                                        .ToListAsync();
            var result = new Pagination<Product>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
