﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Infrastructures.Repositories
{
    public class ShopCategoryRepository : GenericRepository<ShopCategory>, IShopCategoryRepository
    {
        public ShopCategoryRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}
