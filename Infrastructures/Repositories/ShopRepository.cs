﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ShopRepository : GenericRepository<Shop>, IShopRepository
    {
        public ShopRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}