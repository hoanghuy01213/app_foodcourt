﻿using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext appDBContext, ICurrentTimeService currentTime, IClaimService claimService) : base(appDBContext, currentTime, claimService)
        {
        }
    }
}
