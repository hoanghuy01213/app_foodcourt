﻿using Application;
using Application.IRepositories;
using Application.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly IShopRepository _shopRepository;
        private readonly IShopCategoryRepository _shopCategoryRepository;
        private readonly ITransactionCounterRepository _transactionCounterRepository;
        private readonly ICardTypeRepository _cardTypeRepository;
        private readonly IProductRepository _productRepository;
        private readonly ICardRepository _cardRepository;
        private readonly IWalletRepository _walletRepository;
        private readonly IUserRepository _userRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly ITransactionRepository _transactionRepository;

        private bool _disposed;
        private IDbContextTransaction _transaction;

        public UnitOfWork(AppDbContext dbContext,
            IShopRepository shopRepository,
            IShopCategoryRepository shopCategoryRepository,
            ITransactionCounterRepository transactionCounterRepository,
            ICardTypeRepository cardTypeRepository,
            IProductRepository productRepository,
            ICardRepository cardRepository,
            IWalletRepository walletRepository,
            IUserRepository userRepository,
            IOrderRepository orderRepository,
            ITransactionRepository transactionRepository,
            IOrderDetailRepository orderDetailRepository)
        {
            _dbContext = dbContext;
            _shopRepository = shopRepository;
            _shopCategoryRepository = shopCategoryRepository;
            _transactionCounterRepository = transactionCounterRepository;
            _cardTypeRepository = cardTypeRepository;
            _productRepository = productRepository;
            _cardRepository = cardRepository;
            _walletRepository = walletRepository;
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _transactionRepository = transactionRepository;
            _orderDetailRepository = orderDetailRepository;
        }

        public IShopRepository ShopRepository => _shopRepository;

        public IShopCategoryRepository ShopCategoryRepository => _shopCategoryRepository;

        public ITransactionCounterRepository TransactionCounterRepository => _transactionCounterRepository;

        public ICardTypeRepository CardTypeRepository => _cardTypeRepository;

        public IProductRepository ProductRepository => _productRepository;

        public ICardRepository CardRepository => _cardRepository;

        public IWalletRepository WalletRepository => _walletRepository;

        public IUserRepository UserRepository => _userRepository;

        public IOrderRepository OrderRepository => _orderRepository;

        public ITransactionRepository TransactionRepository => _transactionRepository;

        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository;

        public int SaveChanges() => _dbContext.SaveChanges();

        public async Task<int> SaveChangesAsync() => await _dbContext.SaveChangesAsync();

        public void BeginTransaction()
        {
            _transaction = _dbContext.Database.BeginTransaction();
        }
        public void Commit()
        {
            try
            {
                _dbContext.SaveChanges();
                _transaction?.Commit();
            }
            finally
            {
                _transaction?.Dispose();
                _transaction = null;
            }
        }
        public async Task CommitAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
                _transaction?.Commit();
            }
            finally
            {
                _transaction?.Dispose();
                _transaction = null;
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _transaction?.Dispose();
                    _dbContext.Dispose();
                }
                _disposed = true;
            }
        }
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public async Task ExecuteTransactionAsync(Action action)
        {
            using var transaction = await _dbContext.Database.BeginTransactionAsync();
            try
            {
                action();
                await _dbContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        public void Rollback()
        {
            _transaction?.Rollback();
            _transaction?.Dispose();
            _transaction = null;
        }
    }
}
